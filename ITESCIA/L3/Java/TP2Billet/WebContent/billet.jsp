<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="fr.univlyon1.m1if.m1if03.classes.*" %>
<%@ page import="fr.univlyon1.m1if.m1if03.servlets.*" %>
<%@ page import="DAO.*" %>
<%@ page import="java.util.ArrayList" %>
<%! private Billet billet = new Billet();
    private DAOBillet daoB = new DAOBillet();
    private DAOCommentaire daoC = new DAOCommentaire();
    private ArrayList<Commentaire> coms = new ArrayList<Commentaire>();
%>
<%
if(request.getParameter("billet") != null){
	System.out.println(request.getParameter("billet"));
int param = Integer.parseInt(request.getParameter("billet"));
System.out.println("ici");
billet = daoB.getBilletByID(param);
coms = daoC.getAllCommentairesByBillet(billet.getId());
System.out.println(billet);
}
 %>
<!DOCTYPE HTML>
<html>
<head>
    <title>Billet</title>
    <link rel="stylesheet" type="text/css" href="style/style.css">
</head>
<body>

<h2>Hello <%= session.getAttribute("pseudo")%> !</h2>



<p>Ceci est un billet de <%= billet.getAuteur() %></p>
<h1><c:out value="<%= billet.getTitre()%>"/></h1>
<div class="contenu"><%= billet.getContenu()%></div>
<hr>

<h3>Commentaires :</h3>
<%
for (Commentaire com : coms){
	System.out.println(com);
	
	%>

<fieldset>
<legend><%= com.getPseudo() %> :</legend>
<%= com.getCommentaire() %>
</fieldset>
<%} %>
<form method="GET" action="ajoutComm">
    <p>
        <input type="hidden" name="billet" value=<%=billet.getId() %> />
        <input type="text" name="commentaire">
        <input type="submit" value="Envoyer">
        
    </p>
</form>
<p><a href="saisie.html">Saisir un nouveau billet</a></p>
<p><a href="list.jsp">Retour</a>
<p><a href="Deco">Se déconnecter</a></p>

</body>
</html>