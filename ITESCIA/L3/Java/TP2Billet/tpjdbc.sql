-- Adminer 4.7.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `acces`;
CREATE TABLE `acces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prenom` varchar(20) NOT NULL,
  `login` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `statut` varchar(20) NOT NULL,
  `age` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `billet`;
CREATE TABLE `billet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(50) NOT NULL,
  `contenu` varchar(500) NOT NULL,
  `auteur` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `billet` (`id`, `titre`, `contenu`, `auteur`) VALUES
(1,	'TestIndex',	'Billet de test ',	'Admin'),
(2,	'qisot',	'Deuxieme billet de test, je teste aussi si je peux mettre un text plutôt long sans que ça pose de problèmes graphiquement',	'Admin'),
(3,	'3eme',	'comme ça',	'Admin'),
(4,	'TESTEUU',	'BILLET DE TESTEUU',	'Testeu'),
(5,	'Testeeeuuu2',	'BILLET DE TESTEEU 2',	'Testeeeeeuuu'),
(6,	'tyfdjty',	'gjuguj',	'fdufgjfg'),
(39,	'Billet',	'srgsryt\r\n',	'Valentin'),
(40,	'Billet',	'srgsryt\r\n',	'Valentin'),
(41,	'billet',	'ertdsyghe',	'hdhdyh'),
(42,	'billet',	'ertdsyghe',	'hdhdyh'),
(43,	'billet3',	'dfgtezr',	'reydy'),
(44,	'fgbhfdch',	'fghjf',	'reydy');

DROP TABLE IF EXISTS `commentaire`;
CREATE TABLE `commentaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commentaire` varchar(255) NOT NULL,
  `pseudo` varchar(50) NOT NULL,
  `billet` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_foreign` (`billet`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `statut`;
CREATE TABLE `statut` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2020-04-28 14:26:29
