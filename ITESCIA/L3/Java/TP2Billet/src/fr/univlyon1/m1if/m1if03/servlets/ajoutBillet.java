package fr.univlyon1.m1if.m1if03.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.DAOBillet;
import fr.univlyon1.m1if.m1if03.classes.Billet;
import fr.univlyon1.m1if.m1if03.classes.Commentaire;

/**
 * Servlet implementation class ajoutBillet
 */
@WebServlet("/ajoutBillet")
public class ajoutBillet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ajoutBillet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(!request.getParameter("contenu").equals("") && !request.getParameter("titre").contentEquals("")) {
	    DAOBillet daoB = new DAOBillet();
	    String contenu = request.getParameter("contenu");
	    String titre = request.getParameter("titre");
        HttpSession session = request.getSession(true);
        String pseudo = (String) session.getAttribute("pseudo");
        Billet billet = new Billet(-1,titre,contenu,pseudo);
        daoB.addBillet(billet);
		}
        
	    
	    getServletContext().getRequestDispatcher("/list.jsp").forward(request,response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
