package fr.univlyon1.m1if.m1if03.classes;

public class Commentaire {
	int id;
	String commentaire;
	String pseudo;
	int billet;
	public Commentaire(int id, String commentaire, String pseudo,int billet) {
		this.id = id;
		this.commentaire = commentaire;
		this.pseudo = pseudo;
		this.billet = billet;
	}
	
	public String getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBillet() {
		return billet;
	}

	public void setBillet(int billet) {
		this.billet = billet;
	}

	public String toString() {
		return "Commentaire [id=" + id + ", commentaire=" + commentaire + ", pseudo=" + pseudo + ", billet=" + billet
				+ "]";
	}
	
	

}
