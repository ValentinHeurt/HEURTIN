package fr.univlyon1.m1if.m1if03.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.DAOBillet;
import DAO.DAOCommentaire;
import fr.univlyon1.m1if.m1if03.classes.Billet;
import fr.univlyon1.m1if.m1if03.classes.Commentaire;

/**
 * Servlet implementation class ajoutComm
 */
@WebServlet("/ajoutComm")
public class ajoutComm extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ajoutComm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Billet billet = new Billet();
	    DAOCommentaire daoC = new DAOCommentaire();
	    DAOBillet daoB = new DAOBillet();
	    int param = Integer.parseInt(request.getParameter("billet"));
	    billet = daoB.getBilletByID(param);
	    String contenu = request.getParameter("commentaire");
        HttpSession session = request.getSession(true);
        String pseudo = (String) session.getAttribute("pseudo");
	    Commentaire com = new Commentaire(1,contenu,pseudo,billet.getId());
	    
	    daoC.addCommentaire(com);
	    //request.setAttribute("billet", billet.getId());
	    
	    getServletContext().getRequestDispatcher("/billet.jsp").forward(request,response);
	    
	    
	    
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}