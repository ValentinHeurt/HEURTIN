package fr.univlyon1.m1if.m1if03.classes;

public class Billet {
	private int id;
    private String titre, contenu, auteur;

    public Billet() {
    	this.id = -1;
        this.titre = "Rien";
        this.contenu = "Vide";
        this.auteur = "Personne";
    }

    public Billet(int id, String titre, String contenu, String auteur) {
    	this.id = id;
        this.titre = titre;
        this.contenu = contenu;
        this.auteur = auteur;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String toString() {
		return "Billet [id=" + id + ", titre=" + titre + ", contenu=" + contenu + ", auteur=" + auteur + "]";
	}
	
    
}
