package DAO;

import java.sql.*;
import java.util.ArrayList;

import fr.univlyon1.m1if.m1if03.classes.Commentaire;
public class DAOCommentaire {
	private Connection getConnection() throws ClassNotFoundException, SQLException {
		
		String driver = "com.mysql.jdbc.Driver";
		String dbName= "tpJDBC"; 
		String login= "root"; 
		String motdepasse= "root"; 
		String strUrl = "jdbc:mysql://localhost:3306/" +  dbName + "?useSSL=false";

		Class.forName("com.mysql.jdbc.Driver");
		System.out.println("Debug : Driver OK");
		Connection conn = DriverManager.getConnection(strUrl, login, motdepasse);
		System.out.println("Debug : Connection OK");

	    return conn;
	    
	}
	
	public boolean addCommentaire(Commentaire com) {
		boolean flag = false;
		try { 
			Connection conn = getConnection();
			String req = "INSERT INTO commentaire (commentaire,pseudo,billet) VALUES (?,?,?)";
			PreparedStatement pstmt = conn.prepareStatement(req);  
			pstmt.setString(1,com.getCommentaire());
			pstmt.setString(2,com.getPseudo());
			pstmt.setInt(3, com.getBillet());
			pstmt.executeUpdate();
			System.out.println("Debug : Insertion");
			pstmt.close();
			conn.close();
			flag = true;
			}
			catch(ClassNotFoundException e) {  
				System.err.println("Driver non charg� !");  e.printStackTrace();
			} catch(SQLException e) {
				System.err.println("Autre erreur !");  e.printStackTrace();
			}
		return flag;
	}
	
	public ArrayList<Commentaire> getAllCommentaires(){
		ArrayList<Commentaire> list = new ArrayList<Commentaire>();
		try { 
			
			Connection conn = getConnection();
			Statement stUsers = conn.createStatement();  

			ResultSet rsUsers = stUsers.executeQuery("select * from commentaire");  
			while(rsUsers.next()) {
				list.add(new Commentaire(rsUsers.getInt(1),rsUsers.getString(2),rsUsers.getString(3),rsUsers.getInt(4)));
			}  
			conn.close();
			}
			catch(ClassNotFoundException e) {  
				System.err.println("Driver non charg� !");  e.printStackTrace();
			} catch(SQLException e) {
				System.err.println("Autre erreur !");  e.printStackTrace();
			}
		return list;
	}
	

	public ArrayList<Commentaire> getAllCommentairesByBillet(int billet){
		ArrayList<Commentaire> list = new ArrayList<Commentaire>();
		try { 
			
			Connection conn = getConnection();
			Statement stUsers = conn.createStatement();  

			ResultSet rsUsers = stUsers.executeQuery("select * from commentaire WHERE billet = "+ billet);  
			while(rsUsers.next()) {
				list.add(new Commentaire(rsUsers.getInt(1),rsUsers.getString(2),rsUsers.getString(3),rsUsers.getInt(4)));
			}  
			conn.close();
			}
			catch(ClassNotFoundException e) {  
				System.err.println("Driver non charg� !");  e.printStackTrace();
			} catch(SQLException e) {
				System.err.println("Autre erreur !");  e.printStackTrace();
			}
		return list;
	}
	
}
