package DAO;

import java.sql.*;
import java.util.ArrayList;

import fr.univlyon1.m1if.m1if03.classes.Billet;



public class DAOBillet {
	private Connection getConnection() throws ClassNotFoundException, SQLException {
		
		String driver = "com.mysql.jdbc.Driver";
		String dbName= "tpJDBC"; 
		String login= "root"; 
		String motdepasse= "root"; 
		String strUrl = "jdbc:mysql://localhost:3306/" +  dbName + "?useSSL=false";

		Class.forName("com.mysql.jdbc.Driver");
		System.out.println("Debug : Driver OK");
		Connection conn = DriverManager.getConnection(strUrl, login, motdepasse);
		System.out.println("Debug : Connection OK");

	    return conn;
	    
	}
	
	public boolean addBillet(Billet b) {
		boolean flag = false;
		try { 
			Connection conn = getConnection();
			String req = "INSERT INTO billet (titre,contenu,auteur) VALUES (?,?,?)";
			PreparedStatement pstmt = conn.prepareStatement(req);
			pstmt.setString(1,b.getTitre());
			pstmt.setString(2,b.getContenu());
			pstmt.setString(3, b.getAuteur());
			pstmt.executeUpdate();
			System.out.println("Debug : Insertion");
			pstmt.close();
			conn.close();
			flag = true;
			}
			catch(ClassNotFoundException e) {  
				System.err.println("Driver non charg� !");  e.printStackTrace();
			} catch(SQLException e) {
				System.err.println("Autre erreur !");  e.printStackTrace();
			}
		return flag;
	}
	
	public ArrayList<Billet> getAllBillet(){
		ArrayList<Billet> list = new ArrayList<Billet>();
		try { 
			
			Connection conn = getConnection();
			Statement stUsers = conn.createStatement();  

			ResultSet rsUsers = stUsers.executeQuery("select * from billet");  
			while(rsUsers.next()) {
				list.add(new Billet(rsUsers.getInt(1),rsUsers.getString(2),rsUsers.getString(3), rsUsers.getString(4)));
			}  
			conn.close();
			}
			catch(ClassNotFoundException e) {  
				System.err.println("Driver non charg� !");  e.printStackTrace();
			} catch(SQLException e) {
				System.err.println("Autre erreur !");  e.printStackTrace();
			}
		return list;
	}
	
	public Billet getBilletByID(int id) {
		Billet b = new Billet();
		try {
        Connection conn = getConnection();
        
        Statement stAddUser = conn.createStatement();
        ResultSet rs = stAddUser.executeQuery("select * from billet where id=" + id);
		while(rs.next()) {
			b = new Billet(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4));
		}  
		}
        catch(ClassNotFoundException e) {
            System.err.println("Driver non charg� !");  e.printStackTrace();
        } catch(SQLException e) {
            System.err.println("Autre erreur !");  e.printStackTrace();
        }
		return b;
	}
	
	
}
