import json

fichier_entree = open('panorama-des-festivals.json',"r")
fichier_sortie = open('test.json','w')
decode = json.load(fichier_entree)
result=[]

for i in decode:
    temp={}
    temp['nom_de_la_manifestation'] = i.get('fields').get('nom_de_la_manifestation')
    temp['code_postal'] = i.get('fields').get('code_postal')
    temp['site_web'] = i.get('fields').get('site_web')
    temp['date_de_debut'] = i.get('fields').get('date_de_debut')
    temp['date_de_fin'] = i.get('fields').get('date_de_fin')
    temp['commune_principale'] = i.get('fields').get('commune_principale')
    temp['domaine'] = i.get('fields').get('domaine')
    print(temp)
    result.append(temp)

back_json = json.dumps(result)
fichier_sortie.write(back_json)
fichier_sortie.close()
