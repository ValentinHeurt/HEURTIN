class Manifestation:
    def __init__(self,nom,code_postal,site,date_debut,date_fin,commune,domaines):
        self.nom =nom
        self.code_postal = code_postal
        self.site = site
        self.date_debut = date_debut
        self.date_fin = date_fin
        self.commune = commune
        self.domaines = domaines

    def get_nom(self):
        return self.nom
    def set_nom(self,nom):
        self.nom = nom
    def get_code(self):
        return self.code_postal
    def set_code(self,code_postal):
        self.code_postal = code_postal
    def get_site(self):
        return self.site
    def set_site(self,site):
        self.site = site
    def get_dateD(self):
        return self.date_debut
    def set_dateD(self,date_debut):
        self.date_debut = date_debut
    def get_dateF(self):
        return self.date_fin
    def set_dateF(self,date_fin):
        self.date_fin = date_fin
    def get_commune(self):
        return self.commune
    def set_commune(self,commune):
        self.commune = commune
    def get_domaines(self):
        return self.domaines
    def set_domaines(self,domaines):
        self.domaines = domaines



