from tkinter import *
from Manifestation import Manifestation
from fpdf import FPDF
import json



fenetre = Tk()


compteur = 0
nom = Label(fenetre, text='Nom de la manifestation :')
nom.grid(row=2, column=0)
code = Label(fenetre, text='code postal : ')
code.grid(row=3, column=0)
site = Label(fenetre, text='Site web : ')
site.grid(row=4, column=0)
date_debut = Label(fenetre, text='Date de début : ')
date_debut.grid(row=5, column=0)
date_fin = Label(fenetre, text='Date de fin : ')
date_fin.grid(row=6, column=0)
commune = Label(fenetre, text='Commune principale : ')
commune.grid(row=7, column=0)
domaine = Label(fenetre, text='Domaine : ')
domaine.grid(row=8, column=0)

var_nom = StringVar()
rep_nom = Label(fenetre,textvariable = var_nom)
rep_nom.grid(row=2, column=1)
var_code = StringVar()
rep_code = Label(fenetre,textvariable=var_code)
rep_code.grid(row=3, column=1)
var_site = StringVar()
rep_site = Label(fenetre,textvariable=var_site)
rep_site.grid(row=4, column=1)
var_dateD = StringVar()
rep_dateD = Label(fenetre,textvariable=var_dateD)
rep_dateD.grid(row=5, column=1)
var_dateF = StringVar()
rep_dateF = Label(fenetre,textvariable=var_dateF)
rep_dateF.grid(row=6, column=1)
var_commune = StringVar()
rep_commune = Label(fenetre,textvariable=var_commune)
rep_commune.grid(row=7, column=1)
var_domaine = StringVar()
rep_domaine = Label(fenetre,textvariable=var_domaine)
rep_domaine.grid(row=8, column=1)
manifestation = []

l1 = Label(fenetre,text = "Code postal")
l1.grid(row = 0, column = 0)
e1 = Entry(fenetre)
e1.grid(row = 0, column = 1)
l2 = Label(fenetre,text = "Date de debut")
l2.grid(row = 0, column = 2)
e2 = Entry(fenetre)
e2.grid(row = 0, column = 3)
l3 = Label(fenetre,text = "Date de fin")
l3.grid(row = 0, column = 4)
e3 = Entry(fenetre)
e3.grid(row = 0, column = 5)
l4 = Label(fenetre,text = "Domaine")
l4.grid(row = 0, column = 6)
e4 = Entry(fenetre)
e4.grid(row = 0, column = 7)

def getData():
    fichier_json = open("test.json")
    decode = json.load(fichier_json)
    global manifestations
    manifestations = []
    for i in decode:
        status_code_postal = False
        status_date_debut = False
        status_date_fin = False
        status_domaine = False
        if i.get('code_postal') == e1.get() or e1.get() == "":
            status_code_postal = True
        if i.get("date_de_debut") == e2.get() or e2.get() == "":
            status_date_debut = True
        if i.get("date_de_fin") == e3.get() or e3.get() == "":
            status_date_fin = True
        if i.get("domaine") == e4.get() or e4.get() == "":
            status_domaine = True

        if status_date_fin * status_date_debut * status_code_postal * status_domaine:
            manifestation = Manifestation(i.get("nom_de_la_manifestation"), i.get("code_postal"), i.get('site_web'),
                                          i.get("date_de_debut"), i.get("date_de_fin"), i.get("commune_principale"),
                                          i.get("domaine"))
            manifestations.append(manifestation)


    return manifestations


def precedant():
    global compteur
    if compteur == 0 :
            print("nani")
    else:
        global var_domaine
        global var_nom
        global var_commune
        global var_dateF
        global var_dateD
        global var_code
        global var_site
        compteur = compteur - 1

        var_nom.set(manifestations[compteur].get_nom())

        var_code.set(manifestations[compteur].get_code())

        var_site.set(manifestations[compteur].get_site())

        var_dateD.set(manifestations[compteur].get_dateD())

        var_dateF.set(manifestations[compteur].get_dateF())

        var_commune.set(manifestations[compteur].get_commune())

        var_domaine.set(manifestations[compteur].get_domaines())
def suivant():
    global compteur
    if compteur+1 >= len(manifestations):
        print("nani")
    else:
        compteur = compteur + 1
        global var_domaine
        global var_nom
        global var_commune
        global var_dateF
        global var_dateD
        global var_code
        global var_site

        var_nom.set(manifestations[compteur].get_nom())

        var_code.set(manifestations[compteur].get_code())

        var_site.set(manifestations[compteur].get_site())

        var_dateD.set(manifestations[compteur].get_dateD())

        var_dateF.set(manifestations[compteur].get_dateF())

        var_commune.set(manifestations[compteur].get_commune())

        var_domaine.set(manifestations[compteur].get_domaines())





def search():
    compteur = 0


    manifestations = getData()
    if len(manifestations) >= 1:
        global var_domaine
        global var_nom
        global var_commune
        global var_dateF
        global var_dateD
        global var_code
        global var_site

        var_nom.set(manifestations[compteur].get_nom())

        var_code.set(manifestations[compteur].get_code())

        var_site.set(manifestations[compteur].get_site())

        var_dateD.set(manifestations[compteur].get_dateD())

        var_dateF.set(manifestations[compteur].get_dateF())

        var_commune.set(manifestations[compteur].get_commune())

        var_domaine.set(manifestations[compteur].get_domaines())
    else:
        print("nani")


def save():
    pdf = FPDF()
    pdf.add_page()
    pdf.set_font("Arial", size = 12)
    nombre = 0
    for row in manifestations:
        if nombre == 3:
            pdf.add_page()
            nombre = 0
        if row.get_nom() != None:
            text = "Nom de la manifestation : " + row.get_nom()
            text = text.encode('latin-1', 'replace').decode('latin-1')
            pdf.cell(0, 10, txt=text, ln=1, align="L")
        else:
            text = "Nom de la manifestation : None"
            pdf.cell(0, 10, txt=text, ln=1, align="L")

        if row.get_code() != None:
            text = "Code postal : " + row.get_code()
            text = text.encode('latin-1', 'replace').decode('latin-1')
            pdf.cell(0, 10, txt=text, ln=1, align="L")
        else:
            text = "Code postal : None"
            pdf.cell(0, 10, txt=text, ln=1, align="L")

        if row.get_site() != None:
            text = "Site web : " + row.get_site()
            text = text.encode('latin-1', 'replace').decode('latin-1')
            pdf.cell(0, 10, txt=text, ln=1, align="L")
        else:
            text = "Site web : None"
            pdf.cell(0, 10, txt=text, ln=1, align="L")

        if row.get_dateD() != None:
            text = "Date de début : " + row.get_dateD()
            text = text.encode('latin-1', 'replace').decode('latin-1')
            pdf.cell(0, 10, txt=text, ln=1, align="L")
        else:
            text = "Date de début : None"
            pdf.cell(0, 10, txt=text, ln=1, align="L")

        if row.get_dateF() != None:
            text = "Date de fin : " + row.get_dateF()
            text = text.encode('latin-1', 'replace').decode('latin-1')
            pdf.cell(0, 10, txt=text, ln=1, align="L")
        else:
            text = "Date de fin : None"
            pdf.cell(0, 10, txt=text, ln=1, align="L")

        if row.get_commune() != None:
            text = "Commune principale : " + row.get_commune()
            text = text.encode('latin-1', 'replace').decode('latin-1')
            pdf.cell(0, 10, txt=text, ln=1, align="L")
        else:
            text = "Commune principale : None"
            pdf.cell(0, 10, txt=text, ln=1, align="L")

        if row.get_domaines() != None:
            text = "Domaine : " + row.get_domaines()
            text = text.encode('latin-1', 'replace').decode('latin-1')
            pdf.cell(0, 10, txt=text, ln=1, align="L")
        else:
            text = "Domaine : None"
            pdf.cell(0, 10, txt=text, ln=1, align="L")

        pdf.cell(0, 10, txt="", ln=1, align="L")
        nombre = nombre + 1
    pdf.output("resultat.pdf")












b1 = Button(fenetre, text="oui", bg='blue',command=search)
suivant = Button(fenetre, text="Suivant", command=suivant)
precedant = Button(fenetre, text="precedant", command=precedant)
save = Button(fenetre, text="Sauvegarder en pdf", command=save)

b1.grid(row = 0, column = 8)
suivant.grid(row=9,column=1)
precedant.grid(row=9,column=0)
save.grid(row=9,column=8)


mainloop()

