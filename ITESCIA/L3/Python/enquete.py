#Dictionnaire contenant qui affilie les nom des suspect a leur code ADN
suspect = {'Mlle Rose' : "CTCCTGATGCTCCTCGCTTGGTGGTTTGAGTGGACCTCCCAGGCCAGTGCCGGGCCCCTCATAGGAGCAGC" ,
           'Colonel Moutarde' : "CCTGGAGGGTGGCCCCACCGGCCGAGACAGCGAGCATATGCAGGAAGCGGCAGGAATAAGGAAAAGAGG" ,
           "Mme Pervenche" : "AAGCTCGGGAGGTGGCCAGGCGGCAGGAAGGCGCACCCCCCCAGTACTCCGCGCGCCGGGACAGAATGCC",
           "M LeBlanc" : "CTGCAGGAACTTCTTCTGGAAGTACTTCTCCTCCTGCAAATAAAACCTCACCCATGAATGCTCACGCAAG"}
#Déclaration des code ADN
codecoup1 = "CATA"
codecoup2 = "ATGC"
#Code ADN qui sert à vérifier que les deux brin si dessus ne sont pas collé et donc sont éloignés
noncolle = "CATATGC"

#On parcour le dictionnaire
for i in suspect:
    #On vérifie que le code ADN de la personne comprenne CATA et ATGC mais également qu'il ne contienne pas CATATGC.
    if codecoup1 in suspect[i] and codecoup2 in suspect[i] and noncolle not in suspect[i]:
        #On vérifie si le ou la coupable est un homme ou une femme pour écrire soit le soit la
        if i == "Mlle Rose" or i == "Mme Pervenche":
            print("La coupable est " + i)
        else:
            print("Le coupable est " + i)




