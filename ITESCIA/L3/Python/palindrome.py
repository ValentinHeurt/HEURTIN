# Cette fonction retourne un booléen indiquant si la chaîne de caractères est un palindrome ou non.
def palindrome(a):

    #Compare la string a avec son inverse et retourne True si ces deux valeurs sont égales sinon, retourne False.
    return a == a[::-1]

# Demande à l'utilisateur d'entrer un mot et le stock dans a.
a = input("entrez un mot : ")

# Affiche le résultat de la fonction palindrome.
print(palindrome(a))
