# On initialise deux variables qui vont récuperer deux mots saisis par l'utilisateur
a = input("Veuillez entrer un mot svp : ")
b = input("Un autre svp ^^ : ")

# Fonction distance_hamming qui prend en arguments deux variables.
def distance_hamming(a,b):
    # Si les variables a et b sont de mêmes longueur, la distance de hamming est calculée.
    if len(a) == len(b):
        # Compteur utilisé pour le résultat final.
        hamming = 0
        # On boucle sur la variable a ( qui est forcement de même longueur que b )
        for i in range (0, len(a)):
            # On incrémente de 1 le compteur "hamming" si la lettre d'indice "i" des variables a et b sont différentes
            if a[i] != b[i]:
                hamming = hamming +1
        return hamming
    else:
        # Message d'érreur dans le cas ou les deux mots entrés en argument ne sont pas de même longueur.
        return "Les deux mots entrés dans cette fonction doivent être de même longueur."
# Test de la fonction
print(distance_hamming(a,b))

