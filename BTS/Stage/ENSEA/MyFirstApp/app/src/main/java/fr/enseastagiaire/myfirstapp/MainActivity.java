package fr.enseastagiaire.myfirstapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class MainActivity extends AppCompatActivity {


    private Button go;
    private Button go2;
    private Button go3;
    private ImageView pref;
    private TextView toto;
    private String test;
    private  Context mContext;
    private String batiment;
    private String etage;
    private String zone;
    String url="http://34.244.94.135/ensea/bdd.php"; // La variable url doit être égale a l'url menant à vôtre fichier "bdd.php"



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;

        final SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(this);
        this.toto = findViewById(R.id.titre);


        this.go = findViewById(R.id.button_yes); // affecte le bouton button_yes a la variable go


        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent otherActivity = new Intent(MainActivity.this, page2.class);
                startActivity(otherActivity);


            }
        });



        this.go2 = findViewById(R.id.button_no);

        go2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder constructeur = new AlertDialog.Builder(mContext);
                constructeur.setCancelable(true);
                constructeur.setTitle("Confirmation");
                constructeur.setMessage("demande de confirmation test");
                constructeur.setPositiveButton("Confirmer", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (android.os.Build.VERSION.SDK_INT > 9) {
                                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                                    StrictMode.setThreadPolicy(policy);
                                }
                                final SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                                batiment = preference.getString("bati","error");
                                etage = preference.getString("etage","error");
                                zone = preference.getString("zone","PasD");

                                ArrayList<NameValuePair> param = new ArrayList<NameValuePair>(3);
                                param.add(new BasicNameValuePair("batiment", batiment));
                                param.add(new BasicNameValuePair("etage", etage));
                                param.add(new BasicNameValuePair("statut", "invalide"));
                                param.add(new BasicNameValuePair("zone", zone));

                                TimeZone tz = TimeZone.getDefault();
                                Calendar c = Calendar.getInstance(tz);

                                Date dateActu = c.getTime();

                                DateFormat form = new SimpleDateFormat("yyyy-MM-dd");
                                String date = form.format(dateActu);


                                DateFormat hours = new SimpleDateFormat("HH:mm:ss");
                                hours.setTimeZone(TimeZone.getTimeZone("GMT+2"));
                                String heure = hours.format(dateActu);


                                param.add(new BasicNameValuePair("date", date ));
                                param.add(new BasicNameValuePair("heure",heure));
                                try {
                                    boolean verif = true;
                                    if(batiment.equals("error") || etage.equals("error") || etage.equals("") || batiment.equals("") || etage.equals("1")){
                                        verif = false;
                                    }
                                    if(batiment.equals("D"))
                                        if(zone.equals("PasD") || zone.equals("") || zone.equals("1")){
                                            verif = false;
                                        }
                                    if(verif == true) {
                                        HttpClient httpClient = new DefaultHttpClient();
                                        HttpPost httpPost = new HttpPost(url);
                                        httpPost.setEntity(new UrlEncodedFormEntity(param));
                                        HttpResponse response = httpClient.execute(httpPost);
                                        Intent sortir = new Intent(MainActivity.this, page5.class);
                                        startActivity(sortir);
                                    }
                                    else{
                                        AlertDialog.Builder error = new AlertDialog.Builder(MainActivity.this);
                                        error.setCancelable(true);
                                        error.setTitle("Erreur");
                                        error.setMessage("Vous n'avez pas spécifier vôtre batiment ou vôtre étage");
                                        error.setPositiveButton("Compris", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }

                                        });
                                        AlertDialog fail = error.create();
                                        fail.show();
                                    }

                                } catch (Exception e) {
                                    Log.e("log_tag", "Error converting result " + e.toString());
                                }

                            }
                        });
                constructeur.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog dialog = constructeur.create();
                dialog.show();
            }
        });



        this.go3 = findViewById(R.id.button_appel);

        go3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                test = preference.getString("bati","");
                toto.setText(test);


            }
        });

        this.pref = findViewById(R.id.pref);

        pref.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent choixpref = new Intent(getApplicationContext(), pref.class);
                startActivity(choixpref);
            }

        });
    }
    }
