package fr.enseastagiaire.myfirstapp;


import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class page2 extends AppCompatActivity
{


    private Button play3;
    public String statut;
    private String batiment;
    private String etage;
    private String zone;
    String message;
    String url="http://34.244.94.135/ensea/bdd.php";

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page2);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        final SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(this);
        batiment = preference.getString("bati","error");
        etage = preference.getString("etage","error");
        zone = preference.getString("zone","PasD");



        this.play3 = findViewById(R.id.play3);

            play3.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    statut = "valide";
                    ArrayList<NameValuePair> param = new ArrayList<NameValuePair>(3);
                    param.add(new BasicNameValuePair("batiment", batiment));
                    param.add(new BasicNameValuePair("etage", etage));
                    param.add(new BasicNameValuePair("statut", statut));
                    param.add(new BasicNameValuePair("zone", zone));

                    TimeZone tz = TimeZone.getDefault();
                    Calendar c = Calendar.getInstance(tz);
                    Date dateActu = c.getTime();
                    DateFormat form = new SimpleDateFormat("yyyy-MM-dd");
                    String date = form.format(dateActu);

                    DateFormat hours = new SimpleDateFormat("HH:mm:ss");
                    hours.setTimeZone(TimeZone.getTimeZone("GMT+2"));
                    String heure = hours.format(dateActu);

                    param.add(new BasicNameValuePair("date", date ));
                    param.add(new BasicNameValuePair("heure",heure));

                    try {
                        boolean verif = true;
                        if(batiment.equals("error") || etage.equals("error") || etage.equals("") || batiment.equals("") || etage.equals("1")){
                            verif = false;
                        }
                        if(batiment.equals("D"))
                            if(zone.equals("PasD") || zone.equals("") || zone.equals("1")){
                                verif = false;
                            }
                        if(verif == true) {
                            HttpClient httpClient = new DefaultHttpClient();
                            HttpPost httpPost = new HttpPost(url);
                            httpPost.setEntity(new UrlEncodedFormEntity(param));
                            HttpResponse response = httpClient.execute(httpPost);
                            Intent sortir = new Intent(page2.this, page5.class);
                            startActivity(sortir);
                        }
                        else{
                            AlertDialog.Builder error = new AlertDialog.Builder(page2.this);
                            error.setCancelable(true);
                            error.setTitle("Erreur");
                            error.setMessage("Vous n'avez pas spécifier vôtre batiment ou vôtre étage");
                            error.setPositiveButton("Compris", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }

                            });
                            AlertDialog fail = error.create();
                            fail.show();
                        }

                    } catch (Exception e) {
                        Log.e("log_tag", "Error converting result " + e.toString());
                    }

                }
            });
        }
    }