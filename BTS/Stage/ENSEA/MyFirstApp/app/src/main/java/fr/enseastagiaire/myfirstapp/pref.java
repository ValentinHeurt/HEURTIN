package fr.enseastagiaire.myfirstapp;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.app.ActionBar;
import android.widget.Toolbar;

public class
pref extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
    }

    public static class MyPreferenceFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);

            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
            final SharedPreferences.Editor editor = sp.edit();

            final ListPreference batiment = (ListPreference) findPreference("bati");
            final ListPreference etage = (ListPreference) findPreference("etage");
            final ListPreference zone = (ListPreference) findPreference("zone");
            zone.setEnabled(false);


            if( sp.getString("bati","-1").equals("D")) {
                etage.setEntries(R.array.choix_etage_D);
                etage.setEntryValues(R.array.choix_etage_D);
                etage.setSummary("Choisissez vôtre étage");
                etage.setTitle("Etage");
                zone.setEnabled(true);

            }
            else{
                if(sp.getString("bati","-1").equals("-1")){
                    etage.setEnabled(false);
                    zone.setEnabled(false);

                }
                else{
                    zone.setEnabled(false);
                }

            }
            batiment.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String val = newValue.toString();
                    int index = batiment.findIndexOfValue(val);
                    if( index == 2 ) {
                        editor.putString("etage",null);
                        editor.commit();
                        etage.setEntries(R.array.choix_etage_D);
                        etage.setEntryValues(R.array.choix_etage_D);
                        etage.setSummary("Choisissez vôtre étage");
                        etage.setTitle("Etage");
                        zone.setEnabled(true);

                    }
                    else{
                        editor.putString("etage",null);
                        editor.commit();
                        etage.setEntries(R.array.choix_etage);
                        etage.setEntryValues(R.array.choix_etage);
                        etage.setSummary("Choisissez vôtre étage");
                        etage.setTitle("Etage");
                        zone.setEnabled(false);
                    }
                    return true;

                }
            });

        }
    }

}