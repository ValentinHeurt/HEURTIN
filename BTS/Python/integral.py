from math import *
def fonc(x):
    return 3/(1 + 125504*exp(-1.9*x))
def integ(a,b,n):
    h = (b-a)/n
    s = 0
    for k in range(n):
        s = s + h*fonc(a+k*h)
    return s
def valmoy(a,b,n):
    return integ(a,b,n)/(b-a)
