<?php
require_once("culture.php");
abstract class Characters{
	protected $id;
	protected $nom;
	protected $naissance;
	protected $mort;
	protected $culture;
	static $compteur = 0;
	function __construct($id,$nom,$naissance,$mort,$culture){
		$this->id = $id;
		$this->nom = $nom;
		$this->naissance = $naissance;
		$this->mort = $mort;
		$this->culture = $culture;
		self::$compteur++;

	}
	public function __toString(){
		return "Nom : ".$this->nom." Date de naissance : ".$this->naissance." date de mort : ".$this->mort." culture : ".$this->culture->getLibelle();
			}

	public function getId(){
		return $this->id;
	}
	public function setId($id){
		$this->id = $id;
	}
/*	public function compteur(){
	$compteur++;
	echo $compteur;
	}*/
	public function getNom(){
		return $this->nom;
	}
	public function setNom($nom){
		$this->nom = $nom;
	}
	public function getNaissance(){
		return $this->naissance;
	}
	public function setNaissance($naissance){
		$this->naissance = $naissance;
	}
	public function getMort(){
		return $this->mort;
	}
	public function setMort($mort){
		$this->mort = $mort;

	}
	public function getCompteur(){
		return $this->compteur;
	}

}