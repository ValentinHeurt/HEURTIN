<?php
require_once("characters.php");
require_once("region.php");
class Noble extends Characters {
	private $conjoint;
	private $pere;
	private $mere;
	private $maison;

	function __construct($id,$nom,$naissance,$mort,$culture,$conjoint,$pere,$mere,$maison){
		parent::__construct($id,$nom,$naissance,$mort,$culture);
		$this->conjoint = $conjoint;
		$this->pere = $pere;
		$this->mere = $mere;
		$this->maison = $maison;

	}
	public function getConjoint(){
		return $this->conjoint;
	}
	public function setConjoint($conjoint){
		$this->conjoint = $conjoint;
	}
	public function getMere(){
		return $this->mere = $mere;
	}
	public function setMere($mere){
		$this->mere = $mere;

	}
	public function getPere(){
		return $this->pere;
	}
	public function setPere($pere){
		$this->pere = $pere;
	}
	public function getMaison(){
		return $this->maison;
	}
	public function setMaison($maison){
		$this->maison = $maison;
	}



}