<?php

class maison {
	private $nom;
	private $devise;
	private $armoiries;
	private $fondation;
	private $region;

	function __construct($nom,$devise,$armoiries,$fondation,$region){
		$this->nom = $nom;
		$this->devise = $devise;
		$this->armoiries = $armoiries;
		$this->fondation = $fondation;
		$this->region = $region;
	}
	function getNom(){
		return $this->nom;
	}

}