<?php

class region {
	private $id;
	private $libelle;

	function __construct($id,$libelle){
		$this->id = $id;
		$this->libelle = $libelle;
	}
	public function getId(){
		return $id;
	}
	public function getLibelle(){
		return $libelle;
	}
}