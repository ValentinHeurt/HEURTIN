Apache annonce la disponibilit� de la version 10.0 de l'EDI NetBeans
https://www.developpez.com/actu/239338/Apache-annonce-la-disponibilite-de-la-version-10-0-de-l-EDI-NetBeans-qui-integre-un-support-pour-le-JDK-11-JUnit-5-PHP-JavaScript-et-Groovy/

Sortie de Gambas 3.12
https://linuxfr.org/news/sortie-de-gambas-3-12

Notepad ++ : la version 7.6.2 de l'�diteur open source pour Windows est disponible


Sortie de Apache NetBeans IDE (incubating) 10.0 avec am�lioration du support de Java 11 et de PHP 7.3
https://www.developpez.com/actu/239474/Notepad-plusplus-la-version-7-6-2-de-l-editeur-open-source-pour-Windows-est-disponible-avec-l-estampille-edition-gilets-jaunes/

Les outils pour d�veloppeur Swift
https://www.programmez.com/actualites/les-outils-pour-developpeur-swift-28419

NetBeans 10 supporte les derni�res versions de Java et PHP
https://www.lemondeinformatique.fr/actualites/lire-netbeans-10-supporte-les-dernieres-versions-de-java-et-php-73884.html

Windows 10 : Python 3.7 peut d�sormais �tre t�l�charg� sur Microsoft Store
https://www.developpez.com/actu/239612/Windows-10-Python-3-7-peut-desormais-etre-telecharge-sur-Microsoft-Store-Microsoft-previent-que-cette-version-est-encore-en-cours-d-evaluation/

Richard Stallman : la Free Software Foundation a re�u deux dons d'un million $ chacun
https://www.developpez.com/actu/239522/Richard-Stallman-la-Free-Software-Foundation-a-recu-deux-dons-d-un-million-chacun-ce-qui-permettra-de-continuer-a-promouvoir-le-logiciel-libre/

Windows 10 Insider Preview Build 18309 s'accompagne d'une am�lioration de Windows Sandbox
https://www.developpez.com/actu/239692/Windows-10-Insider-Preview-Build-18309-s-accompagne-d-une-amelioration-de-Windows-Sandbox-pour-mieux-prendre-en-charge-les-ecrans-haute-resolution

