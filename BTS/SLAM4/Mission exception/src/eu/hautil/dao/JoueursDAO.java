package eu.hautil.dao;

import eu.hautil.modele.Joueur;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.time.ZoneId;
import java.sql.ResultSet;
import java.sql.Date;

/**
 * @Author Heurtin Valentin
 * @Version 1
 */
public class JoueursDAO {

    private Connection getConnexion() throws ClassNotFoundException, SQLException{
        Connection conn = null;
        String driver = "org.mariadb.jdbc.Driver"; //TODO à compléter
        String url = "jdbc:mariadb://sio-hautil.eu:3306/heurtv"; //TODO à compléter
        String user="heurtv"; //TODO à compléter
        String pwd = "lolilol123456"; //TODO à compléter
        Class.forName(driver);
        System.out.println("driver ok");
        conn=DriverManager.getConnection(url,user,pwd);
        System.out.println("connection ok");
        return conn;
    }

    /**
     *
     * @param p
     */
    public void insertJoueur(Joueur p) throws DAOException{
        try {
            String req1 = "INSERT INTO JoueurFoot VALUES (?,?,?,?,?,?,?)";
            Connection conn = getConnexion();
            PreparedStatement pstmt = conn.prepareStatement(req1);

            pstmt.setInt(1, p.getNumero());
            pstmt.setString(2, p.getLicence());
            pstmt.setString(3, p.getNom());
            pstmt.setString(4, p.getPrenom());
            pstmt.setString(5, p.getPoste());
            pstmt.setString(6, p.getClub());
            pstmt.setDate(7, new Date(p.getDate().getTime()));
            int res2 = pstmt.executeUpdate();
            System.out.println("nb de modif réalisées : " + res2);
            pstmt.close();
            conn.close();
            // récupérer la connexion (méthode privée de cette classe)
            // préparer une requête d'insertion
            // remplir la requête avec les données de l'objet passé en paramètre
            // exécuter la requête
        } catch (SQLException e) {
            DAOException ex = new DAOException("Problème SQL",e);
            throw ex;

        } catch (ClassNotFoundException e) {
            DAOException ex = new DAOException("Problème Driver",e);
            throw ex;

        }
    }

    /**
     *
     * @param licence
     *
     */
    public void deleteJoueur(String licence) throws DAOException {
        try {
            String req5 = "DELETE FROM JoueurFoot WHERE Licence = ?";
            Connection conn = getConnexion();
            PreparedStatement supr = conn.prepareStatement(req5);
            supr.setString(1, licence);
            int res3 = supr.executeUpdate();
            System.out.println("supr : " + res3);
            supr.close();
            conn.close();
            // récupérer la connexion (méthode privée de cette classe)
            // préparer une requête de suppression
            // remplir la requête avec l'id de l'objet passé en paramètre (p)
            // exécuter la requête
        } catch (SQLException e) {
            DAOException ex = new DAOException("Problème Driver",e);
            throw ex;

        } catch (ClassNotFoundException e) {
            DAOException ex = new DAOException("Problème Driver",e);
            throw ex;

        }
    }

    /**
     *
     * @param licence
     * @return
     *
     */
    public Joueur getJoueurById(String licence) throws DAOException {
        Joueur res = null;
        try {

            String req6 = "SELECT * FROM JoueurFoot WHERE Licence = ?";
            Connection conn = getConnexion();
            PreparedStatement rechNom = conn.prepareStatement(req6);
            rechNom.setString(1, licence);
            ResultSet resId = rechNom.executeQuery();
            if (resId.next()) {
                res = new Joueur(licence, resId.getString(3), resId.getString(4), resId.getString(5), resId.getInt(1), resId.getString(6), resId.getDate(7));
            }
            resId.close();
            rechNom.close();
            conn.close();
            // récupérer la connexion (méthode privée de cette classe)
            // préparer une requête de selection
            // remplir la requête avec la licence passée en paramètre
            // exécuter la requête


        } catch (SQLException e) {
            DAOException ex = new DAOException("Problème",e);
            throw ex;

        } catch (ClassNotFoundException e) {
            DAOException ex = new DAOException("Problème",e);
            throw ex;

        }
        return res;
    }

    /**
     *
     * @return
     *
     */
    public ArrayList<Joueur> getAllJoueurs() throws DAOException{
        ArrayList<Joueur> list = new ArrayList<Joueur>();

        try {
            String req7 = "SELECT * FROM JoueurFoot";
            Connection conn = getConnexion();
            PreparedStatement rechNom = conn.prepareStatement(req7);
            ResultSet resAll = rechNom.executeQuery();
            while (resAll.next()) {
                list.add(new Joueur(resAll.getString(2), resAll.getString(3), resAll.getString(4), resAll.getString(5), resAll.getInt(1), resAll.getString(6), resAll.getDate(7)));
            }
            resAll.close();
            rechNom.close();
            conn.close();
            // récupérer la connexion (méthode privée de cette classe)
            // préparer une requête de selection
            // remplir la requête avec la specialité passé en paramètre (s)
            // exécuter la requête
        } catch (SQLException e) {
            DAOException ex = new DAOException("Problème Driver",e);
            throw ex;

        } catch (ClassNotFoundException e) {
            DAOException ex = new DAOException("Problème Driver",e);
            throw ex;

        }
        return list;

    }


}
