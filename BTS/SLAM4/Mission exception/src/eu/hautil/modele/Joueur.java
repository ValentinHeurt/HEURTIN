package eu.hautil.modele;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
public class Joueur {
    private String licence;
    private String nom;
    private String prenom;
    private String poste;
    private int numero;
    private String club;
    private Date date;


    public Joueur(String licence, String nom, String prenom, String poste, int numero, String club,Date date) {
        this.licence = licence;
        this.nom = nom;
        this.prenom = prenom;
        this.poste = poste;
        this.numero = numero;
        this.club = club;
        this.date = date;

    }

    public String getLicence() { return licence; }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom ;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public String getPoste() {return poste ;}
    public void setPoste(String poste){this.poste = poste;}
    public int getNumero() {return numero;}
    public void setNumero(int numero){ this.numero = numero;}
    public String getClub(){return club;}
    public Date getDate(){ return date;}

    @Override
    public String toString() {
        return "Joueur{" +
                "licence='" + licence + '\'' +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", poste='" + poste + '\'' +
                ", numero=" + numero +
                ", club='" + club + '\'' +
                ", date=" + date +
                '}';
    }
}
