package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setTitle("Hello World");
        primaryStage.setTitle("JavaFX Welcome");
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text scenetitle = new Text("Maj ou Min");
        scenetitle.setId("welcome-text");
        grid.add(scenetitle, 0, 0, 2, 1);

        Label gneu= new Label("Txt :");
        grid.add(gneu, 0, 1);

        TextField champ1 = new TextField();
        grid.add(champ1, 1, 1);

        Button maj = new Button("Maj");
        grid.add(maj, 0, 2);

        Button min = new Button("Min");
        grid.add(min, 1, 2);

        final Text rep = new Text();
        grid.add(rep, 1, 3);
        rep.setId("actiontarget");
        maj.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                rep.setText(champ1.getText().toUpperCase());
                champ1.setText("");
            }
    });
        min.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                rep.setText(champ1.getText().toLowerCase());
                champ1.setText("");


            }
        });

        Scene scene = new Scene(grid, 300, 275);
        primaryStage.setScene(scene);
        scene.getStylesheets().add(Main.class.getResource("Style.css").toExternalForm());
        primaryStage.show();



    }


    public static void main(String[] args) {
        launch(args);
    }
}
