import java.sql.*;
import java.util.Scanner;
public class JBDCPrepare {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args){
        try{
            Class.forName("org.mariadb.jdbc.Driver");
            System.out.println("driver ok");
            Connection conn = DriverManager.getConnection("jdbc:mariadb://sio-hautil.eu:3306/heurtv?user=heurtv&password=lolilol123456");
            System.out.println("connection ok");
            System.out.println("Entrez 1 pour ajouter un prof 2 pour recherche par spe 3 pour supr 4 pour recherche par nom et 0 pour stop");
            int choix = sc.nextInt();
            while(choix != 0) {
                if (choix == 1) {
                    String req3 = "INSERT INTO bts_professeurs VALUES (?,?,?)";
                    PreparedStatement pstmt = conn.prepareStatement(req3);
                    System.out.println("id");
                    int id = sc.nextInt();
                    System.out.println("nom");
                    String nom = sc.next();
                    System.out.println("spe");
                    String spe = sc.next();
                    pstmt.setInt(1, id);
                    pstmt.setString(2, nom);
                    pstmt.setString(3, spe);
                    int res2 = pstmt.executeUpdate();
                    System.out.println("nb de modif réalisées : " + res2);
                }
                if (choix == 2) {
                    System.out.println("recherche spe");
                    String recherche = sc.next();
                    System.out.println(recherche);
                    String req4 = "SELECT * FROM bts_professeurs WHERE specialite = ?";
                    PreparedStatement premiere = conn.prepareStatement(req4);
                    premiere.setString(1, recherche);
                    ResultSet res = premiere.executeQuery();
                    while (res.next()) {
                        System.out.println("Nom : " + res.getString(2));
                        System.out.println("Spécialité : " + res.getString(3));
                    }
                    res.close();
                    premiere.close();
                }
                if (choix == 3) {
                    String req5 = "DELETE FROM bts_professeurs WHERE id = ?";
                    PreparedStatement supr = conn.prepareStatement(req5);
                    System.out.println("id a supr");
                    int choixSupr = sc.nextInt();
                    supr.setInt(1, choixSupr);
                    int res3 = supr.executeUpdate();
                    System.out.println("supr : " + res3);
                }
                if (choix == 4) {

                    System.out.println("recherche par nom");
                    String rechercheNom = sc.next();
                    System.out.println(rechercheNom);
                    String req6 = "SELECT * FROM bts_professeurs WHERE nom = ?";
                    PreparedStatement rechNom = conn.prepareStatement(req6);
                    rechNom.setString(1, rechercheNom);
                    ResultSet res5 = rechNom.executeQuery();
                    while (res5.next()) {
                        System.out.println("Nom : " + res5.getString(2));
                        System.out.println("Spécialité : " + res5.getString(3));
                    }
                    res5.close();
                    rechNom.close();
                }
                System.out.println("Entrez 1 pour ajouter un prof 2 pour recherche par spe 3 pour supr 4 pour recherche par nom et 0 pour stop");
                choix = sc.nextInt();
            }
        }catch(Exception e){
            e.printStackTrace();
        }

    }
}
