import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
public class JBDCSimple {
    public static void main(String[] args){
        try{
            Class.forName("org.mariadb.jdbc.Driver");
            System.out.println("driver ok");
            Connection conn = DriverManager.getConnection("jdbc:mariadb://sio-hautil.eu:3306/heurtv?user=heurtv&password=lolilol123456");
            System.out.println("connection ok");
            Statement stmt = conn.createStatement();
            String req1 = "INSERT INTO bts_professeurs VALUES (32,'Abdelmoula','SLAM')";
            int res = stmt.executeUpdate(req1);
            String req2 = "SELECT nom, specialite FROM bts_professeurs";
            ResultSet result = stmt.executeQuery(req2);
            while(result.next()){
                System.out.println("Nom : "+result.getString(1));
                System.out.println("Spécialité : "+ result.getString(2));
            }
            result.close();
            stmt.close();


        }catch(Exception e){
            e.printStackTrace();
        }

    }
}
