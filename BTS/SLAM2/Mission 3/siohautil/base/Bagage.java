package eu.siohautil.base;
public class Bagage {
    private int num;
    private String couleur;
    private double poid;

    /**
     * Constructeur a 3 paramètes
     * @param num
     * @param coul
     * @param poid
     */
    public Bagage(int num, String coul, double poid){
        this.num = num;
        this.couleur = coul;
        this.poid = poid;
    }

    /**
     * Change le numéro du bagage pour un nouveau numéro
     * @param num
     */
    public void setNum(int num){
        this.num = num;
    }

    /**
     * *Retourne le numéro du bagage
     * @return Retourne le numéro du bagage
     */
    public int getNum(){
        return this.num;
    }

    /**
     * Change la couleur du bagage
     * @param coul
     */
    public void setCouleur(String coul){
        this.couleur = coul;
    }

    /**
     * Retourne la couleur
     * @return retourne la couleur
     */
    public String getCouleur(){
        return this.couleur;
    }

    /**
     * Change le poids du bagage
     * @param poid
     */
    public void setPoid(float poid){
        this.poid = poid;
    }

    /**
     * Retourne le poids du bagage
     * @return Retourne le poids du bagage
     */
    public double getPoid(){
        return this.poid;
    }

    /**
     * Affiche les informations du bagage
     */
    public void afficher(){
        System.out.println("Le poid du bagage est de " + this.poid +" il est de couleur " + this.couleur + " et est le bagage numéro " + this.num);
    }
}
