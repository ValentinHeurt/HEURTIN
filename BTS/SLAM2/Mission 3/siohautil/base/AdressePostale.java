package eu.siohautil.base;
public class AdressePostale {
    private String adresse;
    private String ville;
    private String codeP;
    /**
     * Constructeur a 3 paramètres
     * @param adr Adresse
     * @param ville ville
     * @param code Code postale
     */
    public AdressePostale(String adr, String ville, String code ){
        this.adresse = adr;
        this.ville = ville;
        this.codeP = code;
    }

    /**
     * Change l'adresse actuelle pour une nouvelle adresse
     * @param adr La nouvelle valeur de adresse
     */
    public void setAdre(String adr){
        this.adresse = adr;
    }

    /**
     * Retourne l'adresse
     * @return Retourne l'adresse
     */
    public String getAdre(){
        return this.adresse;
    }

    /**
     * Change la ville actuel
     * @param ville La nouvelle ville
     */
    public void setVille(String ville){
        this.ville = ville;
    }

    /**
     *
     * @return Retourne la ville
     */
    public String getVille() {
        return this.ville;
    }

    /**
     * Change le code postale actuel pour un nouveau code postale
     * @param code Le nouveau code postale
     */
    public void setCode(String code){
        this.codeP = code;
    }

    /**
     * Rtourne le code postale
     * @return Retourne le code postale
     */
    public String getCode(){
        return this.codeP;
    }

    /**
     * Affiche l'adresse la ville et le code postale.
     */
    public void afficher(){
        System.out.println(this.adresse + " " + this.ville + " " +  this.codeP);
    }

}
