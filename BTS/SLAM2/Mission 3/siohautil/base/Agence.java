package eu.siohautil.base;
import java.util.ArrayList;

public class Agence {
    private String nom;
    private AdressePostale adresse;
    private ArrayList <Voyageur> listeV;

    /**
     * Constructeur a 2 paramètres
     * @param nom
     * @param adresse
     */
    public Agence(String nom,AdressePostale adresse){
        this.nom = nom;
        this.adresse = adresse ;
        listeV=new ArrayList<Voyageur>();
        Voyageur voy1 = new Voyageur(18,"Valentin");
        Voyageur voy2 = new Voyageur(19,"Miguel");
        Voyageur voy3 = new Voyageur(18,"Thomas");
        Voyageur voy4 = new Voyageur(17,"Cathie");
        Voyageur voy5 = new Voyageur(20,"Marc");
        listeV.add(voy1);
        listeV.add(voy4);        listeV.add(voy2);
        listeV.add(voy3);

        listeV.add(voy5);
    }

    /**
     * Afficher les informations de l'agence
     */
    public void afficher(){
        System.out.println("L'agence "+this.nom+" se situe a");
        adresse.afficher();
        for(int i=0; i<listeV.size();i++){
            listeV.get(i).afficher();
        }
    }

    /**
     * Retourne le nom de l'agence
     * @return le nom de l'agence
     */
    public String getNom(){
        return this.nom;
    }
    public void setNom(String nom){
        this.nom=nom;

    }

    /**
     * Retourne l'adresse de l'agence
     * @return l'adresse de l'agence
     */
    public AdressePostale getAdresse(){
        return this.adresse;
    }

    /**
     * Change l'adresse de l'agence
     * @param adresse
     */
    public void setAdresse(AdressePostale adresse){
        this.adresse = adresse;
    }

    /**
     * Ajoute un voyageur dans la liste de voyageur de l'agence
     * @param nom
     * @param age
     */
    public void addVoy(String nom,int age){
        Voyageur temp = new Voyageur(age,nom);
        listeV.add(temp);
    }

    /**
     * Recherche un utilisateur de la liste de l'agence
     * @param cherche
     */
    public void rechercheNom(String cherche){
        int compt = 0;
        for(int i=0;i<listeV.size();i++){
            if(cherche.equals(listeV.get(i).getNom())){
                listeV.get(i).afficher();
            }
            else{
                compt++;
            }
        }
        if(compt==listeV.size()){
            System.out.println("Ce nom n'est pas renseigné !");
        }

    }

    /**
     * Suprimme un utilisateur de la liste de l'agence
     * @param supr
     */
    public void supr(String supr){
        int compt = 0;
        for(int i=0;i<this.listeV.size();i++){
            if(supr.equals(this.listeV.get(i).getNom())){
                this.listeV.remove(i);
            }
            else{
                compt++;
            }
        }
        if(compt==this.listeV.size()){
            System.out.println("Cet utilisateur n'existe pas");
        }

    }






}
