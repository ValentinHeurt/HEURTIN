public class Voiture extends Vehicule {
    private int nbPlace;

    public Voiture(String marque, String modele, String couleur, int nbPlace){
        super(marque, couleur, modele);
        this.nbPlace = nbPlace;
    }

    public void demarrer(){
        System.out.println("La voiture " + this.getMarque() + " " + this.getModele() + " " + this.getCouleur() + " démarre.");
    }

    public void freiner(){
        System.out.println("La voiture " + this.getMarque() + " " + this.getModele() + " " + this.getCouleur() + " freine.");
    }

    public void stationner(){
        System.out.println("La voiture " + this.getMarque() + " " + this.getModele() + " " + this.getCouleur() + " stationne.");
    }
}
