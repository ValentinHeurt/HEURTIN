public class Chien extends Animal {
    private String couleur;
    public Chien(String surnom, String type, int age, String couleur) {
        super(surnom, age, type);
        this.couleur = couleur;
    }

    public void aboyer(){
        System.out.println(surnom+" : ouaf");
    }
}
