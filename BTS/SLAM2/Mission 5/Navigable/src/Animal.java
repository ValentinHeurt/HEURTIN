public class Animal {
    protected String surnom;
    protected int age;
    protected String type;

    public Animal(){

    }
    public Animal(String surnom, int age, String type){

    }
    public void afficher(){
        System.out.println(surnom+" est un "+type+" il a "+age+" ans");
    }
    public void setAge(int age){
        this.age = age;
    }
    public int getAge(){
        return this.age;
    }
    public void setSurnom(String surnom){
        this.surnom = surnom;
    }
    public String getSurnom(){
        return this.surnom;
    }
    public void setType(String type){
        this.type = type;
    }
    public String getType(){
        return this.type;
    }
}
