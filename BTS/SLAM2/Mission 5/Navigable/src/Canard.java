
public class Canard extends Animal implements Navigable {
    private int nbPlumes;
    public Canard(String surnom, int age, String type, int nbPlumes){
        super(surnom,age,type);
        this.nbPlumes = nbPlumes;
    }
    public void accoster(){
        System.out.println(surnom+" accoste");
    }
    public void naviguer(){
        System.out.println(surnom+" navigue");
    }
    public void couler(){
        System.out.println(surnom+" coule");
    }


}
