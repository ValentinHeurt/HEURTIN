public class Bateau extends Vehicule implements Navigable {

    private int volume;
    public Bateau(String marque, String modele, String couleur, int volume) {
        super(marque, modele, couleur);
        this.volume = volume;
    }

    public void accoster() {System.out.println("Le bateau " + this.getModele() + " accoste.");}

    public void naviguer() {System.out.println("Le bateau " + this.getModele() + " navigue.");}

    public void couler() {System.out.println("Le bateau " + this.getModele() + " coule.");}

    public void demarrer() {System.out.println("Le bateau " + this.getModele() + " démarre.");}

    public void freiner() {System.out.println("Le bateau " + this.getModele() + " freine.");}
}
