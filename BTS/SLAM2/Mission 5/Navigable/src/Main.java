import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    private static Scanner sc;

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        ArrayList<Vehicule> v = new ArrayList<Vehicule>();
        Voiture v1 = new Voiture("Opel", "Tigra", "Rouge",4);
        Bateau v2 = new Bateau("boston whaler", "345 Conquest Pilothouse","Blanc",195);

        v.add(v1);
        v.add(v2);

        for (Vehicule t : v){
            t.demarrer();
        }

        ArrayList<Animal> a = new ArrayList<Animal>();
        Chien a1 = new Chien("Inna", "Labrador", 4,"Noir");
        Canard a2 = new Canard("Coincoin", 12,"Coin",999);

        a.add(a1);
        a.add(a2);

        for (Animal r : a){
            r.afficher();
        }

        ArrayList<Navigable> n = new ArrayList<Navigable>();
        Bateau n1 = new Bateau("boston whaler", "230 Vantage", "Blanc", 230);
        Canard n2 = new Canard("Piou", 5159, "Coin", 25468424);

        n.add(n1);
        n.add(n2);

        for (Navigable e : n){
            e.naviguer();
        }


    }
}
