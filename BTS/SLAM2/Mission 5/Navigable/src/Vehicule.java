public abstract class Vehicule {
    protected String marque;
    protected String couleur;
    protected String modele;
    public Vehicule(String marque, String modele, String couleur) {
        this.marque = marque;
        this.modele = modele;
        this.couleur = couleur;
    }

    public void demarrer(){

    }
    public void freiner(){

    }
    public String getCouleur(){
        return couleur;
    }


    public String getMarque(){
        return marque;
    }

    public String getModele() {
        return modele;
    }

}
