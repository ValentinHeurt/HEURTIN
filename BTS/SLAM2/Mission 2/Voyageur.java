public class Voyageur {
    private int age;
    private String nom;
    private String cate;
    private AdressePostale adresse;
    private Bagage bag;
    public Voyageur(int age, String nom){
        this.age = age;
        this.nom = nom;
        this.cate = this.cate();
    }
    public Voyageur(){
        this.age = 0;
        this.nom = " ";
    }
    public void afficher(){
        System.out.println(this.nom+" a "+this.age+" ans et c'est un "+this.cate);
        if(this.adresse != null) {
            this.adresse.afficher();
        }
        else{
            System.out.println("L'adresse n'est pas renseigné");
        }
        if(bag != null){
            this.bag.afficher();
        }
        else{
            System.out.println("Pas de bagage");
        }

    }
    public void setAge(int age){
        if(age>=0) {
            this.age = age;
            this.cate = this.cate();
        }
    }
    public void setNom(String nom){
        if(nom.length()>=2) this.nom = nom;
    }
    public AdressePostale getAdrsse(){
        return adresse;
    }
    public void setAdresse(AdressePostale adresse){
        this.adresse = adresse;
    }
    public void setBagage(Bagage bagou){
        this.bag= bagou;
    }
    public Bagage getBagage() {
        return this.bag;
    }
    private String cate() {
        if (this.age < 2) {
            cate = "Nourisson";
        } else {
            if (this.age < 12 && this.age >= 2) {
                cate = "Enfant";
            } else {
                if (this.age >= 12 && this.age < 55) {
                    cate = "Adulte";
                } else {
                    cate = "Senior";
                }
            }
        }
        return cate;
    }
}

