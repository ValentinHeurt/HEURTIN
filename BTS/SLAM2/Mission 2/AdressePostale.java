public class AdressePostale {
    private String adresse;
    private String ville;
    private String codeP;
    public AdressePostale(String adr, String ville, String code ){
         this.adresse = adr;
         this.ville = ville;
         this.codeP = code;
    }

    public void setAdre(String adr){
        this.adresse = adr;
    }

    public String getAdre(){
        return this.adresse;
    }

    public void setVille(String ville){
        this.ville = ville;
    }

    public String getVille() {
        return this.ville;
    }

    public void setCode(String code){
        this.codeP = code;
    }

    public String getCode(){
        return this.codeP;
    }

    public void afficher(){
        System.out.println(this.adresse + " " + this.ville + " " +  this.codeP);
    }

}
