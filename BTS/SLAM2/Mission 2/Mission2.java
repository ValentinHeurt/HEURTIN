public class Mission2 {
    public static void main(String[] arg) {
        AdressePostale miguel = new AdressePostale("10 rue de miguel", "Migueland", "Miguel");
        miguel.afficher();
        miguel.setAdre("5 rue de miguel");
        miguel.setCode("Migou");
        miguel.setVille("Migouland");
        miguel.afficher();
        Voyageur migou = new Voyageur(45, "migmig");
        migou.setAdresse(miguel);
        migou.afficher();
        miguel.setVille("migolo");
        migou.afficher();
        Bagage miga = new Bagage(10,"bleu",15.8);
        migou.setBagage(miga);
        migou.afficher();
        miga.setCouleur("Orange");

    }
}
