public class Bagage {
    private int num;
    private String couleur;
    private double poid;
    public Bagage(int num, String coul, double poid){
        this.num = num;
        this.couleur = coul;
        this.poid = poid;
    }
    public void setNum(int num){
        this.num = num;
    }

    public int getNum(){
        return this.num;
    }

    public void setCouleur(String coul){
        this.couleur = coul;
    }
    public String getCouleur(){
        return this.couleur;
    }
    public void setPoid(float poid){
        this.poid = poid;
    }
    public double getPoid(){
        return this.poid;
    }
    public void afficher(){
        System.out.println("Le poid du bagage est de " + this.poid +" il est de couleur " + this.couleur + " et est le bagage numéro " + this.num);
    }
}
