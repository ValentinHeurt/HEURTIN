public class Voyageur {
    private int age;
    private String nom;
    private String cate;
    public Voyageur(int age, String nom){
        this.age = age;
        this.nom = nom;
        this.cate = this.cate(this.age);
    }
    public Voyageur(){
        this.age = 0;
        this.nom = " ";
    }
    public void afficher(){
        System.out.println(this.nom+" a "+this.age+" ans et c'est un "+this.cate);

    }
    public void setAge(int age){
        if(age>=0) {
            this.age = age;
            this.cate = this.cate(this.age);
        }
    }
    public void setNom(String nom){
        if(nom.length()>=2) this.nom = nom;
    }
    private String cate(int age) {
        if (this.age < 2) {
            cate = "Nourisson";
        } else {
            if (this.age < 12 && this.age >= 2) {
                cate = "Enfant";
            } else {
                if (this.age >= 12 && this.age < 55) {
                    cate = "Adulte";
                } else {
                    cate = "Senior";
                }
            }
        }
        return cate;
    }
}


