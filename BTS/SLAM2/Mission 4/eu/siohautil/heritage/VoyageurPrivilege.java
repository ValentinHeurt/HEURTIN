package eu.siohautil.heritage;
import eu.siohautil.base.Voyageur;
public class VoyageurPrivilege extends Voyageur{
    private int code;
    public VoyageurPrivilege(int age,String nom,int code){
        super(age,nom);
        this.code = code;

    }

    public String toString(){

        return super.toString()+" son code privilège est "+code;
    }
    public int getCode(){
        return this.code;
    }
    public void setCode(int code){
        this.code = code;
    }

}
