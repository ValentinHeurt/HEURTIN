package eu.siohautil.heritage;
import eu.siohautil.base.Voyageur;
public class VoyageurHandicap extends Voyageur {
    private String handi;
     public VoyageurHandicap(int age,String nom,String handi){
        super(age,nom);
        this.handi = handi;

    }
    public String toString(){
         return super.toString()+" son handicap : il est "+handi;
    }
    public void setHandi(String handi){
        this.handi = handi;
    }
    public String getHandi(){
        return handi;
    }
}
