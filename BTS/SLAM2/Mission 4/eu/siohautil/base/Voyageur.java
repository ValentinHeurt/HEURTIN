package eu.siohautil.base;

import eu.siohautil.base.AdressePostale;
import eu.siohautil.base.Bagage;

/** Class Voyageur
* @author val
* @version 0.1
 */
public class Voyageur {
    protected int age;
    protected String nom;
    protected String cate;
    protected AdressePostale adresse;
    protected Bagage bag;
    /**
     * Constructeur
     * @param age l'age du voyageur
     * @param nom Le nom du voyageur
     */
    public Voyageur(int age, String nom){
        this.age = age;
        this.nom = nom;
        this.cate = this.cate();

    }
    public Voyageur(){
        this.age = 0;
        this.nom = " ";
    }

    public String toString(){
        return nom+" a "+age+" ans et est un " + cate +" son adresse est  "+ adresse.toString() + " " + bag.toString();
    }

    /**
     *
     * @param age La nouvelle age de l'utilisateur
     */
    public void setAge(int age){
        if(age>=0) {
            this.age = age;
            this.cate = this.cate();
        }
    }

    /**
     * Donne le nom du voyageur.
     * @return le nom du voyageur
     */
    public String getNom(){
        return this.nom;
    }

    /**
     * Modifie le nom du voyageur
     * @param nom Le nom du voyageur
     */
    public void setNom(String nom){
        if(nom.length()>=2) this.nom = nom;
    }

    /**
     * Donne l'adresse du voyageur.
     * @return l'adresse du voyageur
     */
    public AdressePostale getAdrsse(){
        return adresse;
    }

    /**
     * Modifie l'adresse du voyageur
     * @param adresse La nouvelle adresse du voyageur
     */
    public void setAdresse(AdressePostale adresse){
        this.adresse = adresse;
    }

    /**
     *Modifie le bagage du voyageur.
     * @param bagou Le nouveau bagage du voyageur
     */
    public void setBagage(Bagage bagou){
        this.bag= bagou;
    }

    /**
     * Donne les infos du bagage du voyageur
     * @return info sur le bagage
     */
    public Bagage getBagage() {
        return this.bag;
    }
    private String cate() {
        if (this.age < 2) {
            cate = "Nourisson";
        } else {
            if (this.age < 12 && this.age >= 2) {
                cate = "Enfant";
            } else {
                if (this.age >= 12 && this.age < 55) {
                    cate = "Adulte";
                } else {
                    cate = "Senior";
                }
            }
        }
        return cate;
    }


}