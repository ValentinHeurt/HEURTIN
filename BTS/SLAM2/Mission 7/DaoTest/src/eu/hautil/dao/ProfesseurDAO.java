package eu.hautil.dao;

import eu.hautil.modele.Professeur;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.*;
public class ProfesseurDAO {

    public Connection getConnexion(){
        Connection conn = null;
        String driver = "org.mariadb.jdbc.Driver"; //TODO à compléter
        String url = "jdbc:mariadb://sio-hautil.eu:3306/heurtv"; //TODO à compléter
        String user="heurtv"; //TODO à compléter
        String pwd = "lolilol123456"; //TODO à compléter
        try{
            Class.forName(driver);
            System.out.println("driver ok");
            conn=	DriverManager.getConnection(url,user,pwd);
            System.out.println("connection ok");

        }catch(Exception e){
            e.printStackTrace();
        }
        return conn;
    }

    public void insertProfesseur(Professeur p) throws SQLException {
        String req1 = "INSERT INTO bts_professeurs VALUES (?,?,?)";
        PreparedStatement pstmt = getConnexion().prepareStatement(req1);

        pstmt.setInt(1,p.getId());
        pstmt.setString(2,p.getNom());
        pstmt.setString(3,p.getSpecialite());
        int res2= pstmt.executeUpdate();
        System.out.println("nb de modif réalisées : " + res2);
        // récupérer la connexion (méthode privée de cette classe)
        // préparer une requête d'insertion
        // remplir la requête avec les données de l'objet passé en paramètre (p)
        // exécuter la requête
    }
    public void deleteProfesseur(Professeur p) throws SQLException {
        String req5 = "DELETE FROM bts_professeurs WHERE id = ?";
        PreparedStatement supr = getConnexion().prepareStatement(req5);
        supr.setInt(1,p.getId());
        int res3= supr.executeUpdate();
        System.out.println("supr : " + res3);
        // récupérer la connexion (méthode privée de cette classe)
        // préparer une requête de suppression
        // remplir la requête avec l'id de l'objet passé en paramètre (p)
        // exécuter la requête
    }
    public Professeur getProfesseurById(int id) throws SQLException {
        Professeur res = null;
        String req6 = "SELECT * FROM bts_professeurs WHERE id = ?";
        PreparedStatement rechNom = getConnexion().prepareStatement(req6);
        rechNom.setInt(1,id);
        ResultSet resId = rechNom.executeQuery();
        if(resId.next()) {
            res = new Professeur(id, resId.getString(2), resId.getString(3));
        }
        resId.close();
        rechNom.close();
        // récupérer la connexion (méthode privée de cette classe)
        // préparer une requête de selection
        // remplir la requête avec l'id passé en paramètre
        // exécuter la requête
        // S'il y a des données : les récupérer dans un objet professeur (res)

        return res;
    }
    public ArrayList<Professeur> getProfesseursBySpecialite(String s) throws SQLException {
        ArrayList<Professeur> list = new ArrayList<Professeur>();
        String req7 = "SELECT * FROM bts_professeurs WHERE specialite = ?";
        PreparedStatement rechNom = getConnexion().prepareStatement(req7);
        rechNom.setString(1,s);
        ResultSet resSpe = rechNom.executeQuery();
        while(resSpe.next()){
            list.add(new Professeur(resSpe.getInt(1),resSpe.getString(2),resSpe.getString(3)));
        }
        resSpe.close();
        rechNom.close();
        // récupérer la connexion (méthode privée de cette classe)
        // préparer une requête de selection
        // remplir la requête avec la specialité passé en paramètre (s)
        // exécuter la requête
        // S'il y a des données : les récupérer dans un objet professeur
        //                         ajouter le professeur à la liste (list)
        return list;
    }
}
