import java.util.Scanner;

public class Remise {
	public static void main (String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Quel produit voulez vous acheter ?");
		String prod = sc.nextLine();
		System.out.println("Combien coute vôtre produit a l'unité hors taxes");
		double ht = sc.nextDouble();
		System.out.println("Combiens de produit voulez vous acheter");
		int nb = sc.nextInt();
		System.out.println("Saisissez 1 si vôtre produit est de première nécessité, sinon saisissez 2.");
		int typeTVA = sc.nextInt();
		double ttc;
		if(typeTVA==1){
		   ttc = (ht*nb)*(1.05);
		}
		else{
		   ttc = (ht*nb)*(1.20);
		}
		if(nb>=100){
			ttc = 0.9*ttc;
		}
		if(nb == 1){
			System.out.print("Votre " + prod + " va couter " + ttc + "€ TTC");
		}
		else {
			System.out.print("Vos " + prod + "s vont couter " + ttc +"€ TTC");
		}
		sc.close();
	}
}

