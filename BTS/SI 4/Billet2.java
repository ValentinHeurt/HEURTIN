import java.util.Scanner;
public class Billet {
    public static void main(String[] args) {
        int conti;
        double total = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Voulez vous continuer vos achats ou quitter l'application ? tapez 1 pour continuer vos achat, sinon tapez 2");
        conti = sc.nextInt();
        while(conti==1) {
            System.out.println("Quel age avez vous ?");
            int age = sc.nextInt();
            double prix;
            String cate;
            while (age < 0) {
                System.out.println("Vous avez fais une faute de frappe, votre age ne peut pas être négative.");
                System.out.println("Quel age avez vous ?");
                age = sc.nextInt();
            }
            if (age <= 3) {
                System.out.println("Vous êtes considéré comme un nourisson, votre billet coutera 5€");
                cate = "nourisson";
            } else {
                if (age > 3 && age <= 12) {
                    System.out.println("Vous êtes considéré comme un enfant, vous beneficiez d'une réduction de 70%");
                    cate = "enfant";
                } else {
                    if (age > 55) {
                        System.out.println("Vous êtes considéré comme un senior, vous beneficiez d'une réduction de 50%");
                        cate = "senior";
                    } else {
                        System.out.println("Vous êtes considéré comme un adulte, vous ne beneficiez pas d'une réduction");
                        cate = "adulte";
                    }
                }
            }
            System.out.println("Veuillez sélectionner un spectacle");
            System.out.println("Les billets du spectacle 1 coutent 50€");
            System.out.println("Les billets du spectacle 2 coutent 100€");
            System.out.println("Les billets du spectacle 3 coutent 150€");
            System.out.println("Les billets du spectacle 4 coutent 200€");
            int choix = sc.nextInt();
            while (choix < 1 || choix > 4) {
                System.out.println("Ce spéctacle n'existe pas, veuillez retaper le numéro du spectacle désiré.");
                choix = sc.nextInt();
            }
            if (choix == 1) {
                System.out.println("Vous avez choisi le spectacle 1");
                prix = 50;

            } else {
                if (choix == 2) {
                    System.out.println("Vous avez choisi le spectacle 2");
                    prix = 100;
                } else {
                    if (choix == 3) {
                        System.out.println("Vous avez choisi le spectacle 3");
                        prix = 150;
                    } else {
                        System.out.println("Vous avez choisi le spectacle 4");
                        prix = 200;
                    }

                }
            }
            if (cate.equals("senior")) {
                prix = prix * 0.5;
            } else {
                if (cate.equals("nourisson")) {
                    prix = 5;
                } else {
                    if (cate.equals("enfant")) {
                        prix = prix * 0.3;
                    }
                }


            }

            System.out.println("Votre billet est au prix de " + prix + "€");
            total = total + prix;
            System.out.println("Le coût total de vos billet sera de " + total + "€");
            System.out.println("Voulez vous continer vos achats ? tapez 1 pour continer sinon, tapez n'importe quel autre touche");
            conti = sc.nextInt();
        }
        }
    }
    /*



     */