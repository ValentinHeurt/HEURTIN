import java.util.Scanner;
public class jeumath1 {
    public static void main (String[] args){
        Scanner sc = new Scanner(System.in);
        int nb;
        String res = "";
        int rep=0;
        for(int indice=1 ; indice<=10 ; indice++){
            System.out.println("Entrez un nombre entier");
            nb = sc.nextInt();
            if(indice == 10){
                res = res + nb;
                rep = rep + nb;
            }
            else {
                res = res + nb + " + ";
                rep = rep + nb;
        }
        }
        System.out.println(res + " = " + rep);
    }
}
/*
Variables :   Entiers : nb , rep , indice
              Chaine de caracteres : res
Début :
              rep prend la valeur 0
              Pour indice de 1 a 10 faire
                   Saisir nb
                   Si indice = 10
                     Alors
                     res prend la valeur res + nb
                     rep prend la valeur rep + nb
                   Sinon
                      res prend la valeur res + nb + "+"
                      rep prend la valeur rep + nb
                   FinSi
                   Afficher res + "=" + rep
                   



*/
