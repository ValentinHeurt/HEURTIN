import java.util.Scanner;
public class Bonus {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Entrez un entier");
        int nb = sc.nextInt();
        if(nb%2==0){
            System.out.println("Cet entier est un nomnbre paire");
        }
        else {
            System.out.println("Cet entier est un nomnbre impaire");
        }
    }
}

/*
Variables : nb
  Debut
      saisir nb
        Si nb%2 = 0
         alors
           Dire "Cet entier est un nombre paire"
        Sinon
              Dire "Cet entier est un nombre impaire"
        FinSi
 */