public class M8 {
    public static void main(String[] args) {
        String nom[] = {"Miguel", "Toto", "Henry", "Jean-mi", "Pierre-henry"};
        int age[] = {47, 2, 55, 90, 85};
        for (int i = 0; i < 5; i++) {
            M8.desc(nom[i], age[i]);
        }
        for (int i = 0; i < 5; i++) {
            System.out.println(nom[i] + " est un " + M8.cate(age[i]));
        }
        System.out.println("La moyenne d'age du groupe est de " + M8.moy(age) + " ans");
        System.out.println("Le plus jeune membre du groupes a " + M8.min(age) + " ans");
        System.out.println("Le plus vieux membre du groupes a " + M8.max(age) + " ans");
    }

    static void desc(String nom, int age) {
        System.out.println(nom + " à " + age + " ans");
    }

    static String cate(int age) {
        String cate;
        if (age < 2) {
            cate = "Nourisson";
        } else {
            if (age < 12 && age >= 2) {
                cate = "Enfant";
            } else {
                if (age >= 12 && age < 55) {
                    cate = "Adulte";
                } else {
                    cate = "Senior";
                }
            }
        }
        return cate;
    }

    static float moy(int[] age) {
        float moy = 0;
        for(int i = 0 ; i<age.length; i++){
            moy = moy + age[i];
        }
        moy = moy/age.length;
        return moy;

    }
    static int min(int[] age) {
        int min = age[0];
        for(int i = 0 ; i<age.length ; i++) {
            if (age[i] < min) {
                min = age[i];
            }
        }
        return min;
    }
    static int max(int[] age) {
        int max = age[0];
        for(int i = 0 ; i<age.length ; i++) {
            if (age[i] > max) {
                max = age[i];
            }
        }
        return max;
    }

}


/*
ALGO : Desc
Entrée : entier : age. Chaine de charactères : nom
DEBUT :
         Afficher nom, ' a ', age, ' ans'
FIN
ALGO : min
Entrée :tableau d'entier : age
Variables : entier : min
DEBUT : 
         min = age[0]
         Pour i allant de 0 à longueur.age ; pas = 1 :
                 Si age[i]< min
                 Alors
                    min = age[i];
                FinSi
        FinPour
        On retourne min;
FIN
ALGO : max
Entrée :tableau d'entier : age
Variables : entier : max
DEBUT : 
         max = age[0]
         Pour i allant de 0 à longueur.age ; pas = 1 :
                 Si age[i]> max
                 Alors
                    max = age[i];
                FinSi
        FinPour
        On retourne max;
FIN          
ALGO : moy
Entrée : Tableau D'entier : age
Variables réel : moy
DEBUT :
        moy = 0
        Pour i allant de 0 à longueur de age ; pas = 1 :
                moy = moy + age[i]
        FinPour
        moy = moy/longueur de age
        On retourne moy
FIN

ALGO : cate
Entrée : Entiers : age
Variables : Chaine de charactères : cate
DEBUT :
        Si age < 2 Alors :
            cate = "Nourisson"
        Sinon
           Si age < 12 ET age >= 2 Alors :
              cate = "Enfant"
           Sinon
              Si age >= 12 ET age < 55 Alors :
                  cate = "Adulte"
              Sinon :
                 cate = "Senior"
              FinSi
           FinSi
        FinSi
        On retourne cate
FIN

ALGO : main
Variables : Tableau de chaine de charactères : nom
            Tableau d'entiers : age
DEBUT :
        nom[] = {"Miguel", "Toto", "Henry", "Jean-mi", "Pierre-henry"}
        age[] = {47, 2, 55, 90, 85}
        Pour i allant de 0 à 5 ; pas = 1 :
             algo.desc(nom[i],age[i])
        FinPour
        Pour i allant de 0 à 5 ; pas = 1 :
             Afficher nom[i] , " est un " , algo.cate(age[i])
        FinPour
        Afficher "La moyenne d'age du groupe est de " , algo.moy(age), " ans"
        Afficher "Le plus jeune membre du groupes a " , algo.min(age), " ans"
        Afficher "Le plus vieux membre du groupes a " , algo.max(age), "ans"
FIN
        
*/

