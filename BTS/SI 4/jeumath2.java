import java.util.Scanner;
public class jeumath2 {
    public static void main (String[] args){
        Scanner sc = new Scanner(System.in);
        int nb = 0;
        String res = "";
        int rep = 0;
        String choix;
        for(int indice=1 ; indice<=5 ; indice++){
            System.out.println("Entrez un entier");
            nb = sc.nextInt();
            System.out.println("Pour addition tape + pour soustraction tape -");
            choix = sc.next();
            if(choix.equals("+")) {
                if (indice == 5){
                    res = res + nb;
                    rep = rep + nb;
            }
            else{
                res = res + nb + " + ";
                rep = rep + nb;
            }
            }
            else{
                if(choix.equals("-")){
                    if (indice == 5) {
                        res = res + nb;
                        rep = rep - nb;
                    }
                    else {
                        res = res + nb + " - ";
                        rep = rep - nb;
                    }
                }
            }
        }
        System.out.println(res + " = " + rep);
    }
}

/*
VARIABLES :  Entier : nb , rep , indice
             Chaine de caractères : res , choix
Debut :
             Pour indice allant de 1 à 5 faire :
              saisir nb
              saisir choix
              Si choix = +
              Alors
                 Si indice = 5
                   Alors
                     res prend la valeur res + nb
                     rep prend la valeur rep + nb
                   Sinon
                     res prend la valeur res + nb + "+"
                     rep prend la valeur rep + nb
                 FinSi*
              Sinon
                   Si choix = -
                       Alors
                            Si indice = 5
                               Alors
                                 res prend la valeur res + nb
                                 rep prend la valeur rep - nb
                             Sinon
                                 res prend la valeur res + nb + "-"
                                 rep prend la valeur rep - nb

                             FinSi
                   Finsi

            Afficher res + "=" + rep



 */