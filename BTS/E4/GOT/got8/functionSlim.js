var nombrePerso;
var nombreVille;
var cultures;
var housesOrder;
$(document).ready(function(){

$("#btn-culture").click(function(event) {
  var select = $("#cultures").children("option:selected").val();
  $("#resultat").bootstrapTable("destroy");
  $("#resultat").bootstrapTable({
url: "http://localhost/got8/index.php/nomparculture?cultures="+select,
columns: [{
field: "cult",
title: 'Culture'
}, {
field: 'name',
title: 'Nom'
},
]
});

});

     $("#btn-valide4").click(function(event) {               
                   $.ajax({ 
                    type: "GET",
                    url: "http://localhost/got8/index.php/obtentionToken",
                    data: "login="+ $("#login").val() +"&pass="+ $("#pass").val(),
                    success: function(data){
                          sessionStorage.setItem('token', data);
                          document.location.href="http://localhost/got8/got5.html";                              
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {      
                            $(".form-group-password").addClass("has-danger");
                            $("#password").addClass("form-control-danger");
                            alert("Ce compte n'existe pas");
                    }             
                    });
    });
     $('#btn-valide').click(function(){ 
    let login=$('#login').val();
      $.ajax({ 
      type: "DELETE",
      url: "http://localhost/got8/index.php/user?token="+sessionStorage.getItem("token")+"&login="+login,
      success: function(data){  
        alert(data);
      },
      error: function(){
        alert("Vous n'êtes pas log");
      }
    });
  });
     $('#btn-del-perso').click(function(){ 
    let id=$('#id-del-perso').val();
      $.ajax({ 
      type: "DELETE",
      url: "http://localhost/got8/index.php/perso?token="+sessionStorage.getItem("token")+"&id="+id,
      success: function(data){  
        alert(data);
      },
      error: function(){
        alert("Vous n'êtes pas log");
      }
    });
  });
  $('#btn-valide2').click(function(){
    let user=$('#user').val();
    let mail=$('#mail').val();
      $.ajax({
        type: "PUT",
        url: "http://localhost/got8/index.php/user?user="+user+"&mail="+mail+"&token="+sessionStorage.getItem('token'),
        success: function(data){
          alert(data);
        },
        error: function(){
        alert("Vous n'êtes pas log");
      }
      });
  });
  $('#btn-new-perso').click(function(){
    let name=$('#new-name').val();
    let actor=$('#new-actor').val();
    let id=$('#id-perso').val();
      $.ajax({
        type: "PUT",
        url: "http://localhost/got8/index.php/perso?id="+id+"&actor="+actor+"&name="+name+"&token="+sessionStorage.getItem('token'),
        success: function(data){
          alert(data);
        },
        error: function(){
        alert("Vous n'êtes pas log");
      }
      });
  });
  $('#btn-valide3').click(function(){
    $('#result3').bootstrapTable({
url: 'http://localhost/got8/index.php/users',
columns: [{
field: 'id',
title: 'id'
}, {
field: 'login',
title: 'Pseudo'
},
]
});
  });
    $('#btn-add').click(function(){ 
 let login=$('#login-add').val();
 let mdp=$('#mdp-add').val();
 let mail=$('#mail-add').val();
 if(login!=null && mdp!=null && mail!=null && login!="" && mdp!="" && mail!=""){
 $.ajax({ 
    type: "POST",
    contentType: 'application/json; charset=utf-8',
    url: "http://localhost/got8/index.php/sign?login="+login+"&mdp="+mdp+"&mail="+mail+"&token="+sessionStorage.getItem("token"),
    success: function(data){
       alert(data);
       },
       error: function(){
        alert("Vous n'êtes pas log");
       }
     });
}else{
  alert("Veuillez entrer toute les informations");
}
});
$('#btn-add-perso').click(function(){ 
let id=$('#id-add-perso').val();
let name=$('#name-add-perso').val();
let actor=$('#actor-add-perso').val();
$.ajax({ 
type: "POST",
contentType: 'application/json; charset=utf-8',
url: "http://localhost/got8/index.php/perso?id="+id+"&name="+name+"&actor="+actor+"&token="+sessionStorage.getItem("token"),
success: function(data){
alert(data);
},
error: function(){
        alert("Vous n'êtes pas log");
      }
});
});
    $('#btn-delete-ville').click(function(){
      let ville=$('#ville').val();
      $.ajax({ 
      type: "DELETE",
      url: "http://localhost/got8/index.php/ville?token="+sessionStorage.getItem("token")+"&ville="+ville,
      success: function(data){  
        alert(data);
      },
      error: function(){
        alert("Vous n'êtes pas log");
      }
    });

    });
    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    };
     $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        url: "http://localhost/got8/index.php/nbperso",
        success: function(data){
            nombrePerso = JSON.parse(data);
        }

        });
    $('#btn-stats').click(function(){
        
        var nb = new Array();
        var maison = new Array();
        var backgroundColor = new Array();
        for (var i = 0; i < nombrePerso.length; i++) {
          nb[i] = nombrePerso[i].nb;
          maison[i] = nombrePerso[i].name;
          backgroundColor[i] = getRandomColor();
        }
        var ctx = document.getElementById("stats").getContext('2d');
        var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: maison,
            datasets: [{
                label: 'Perso par maison',
                data: nb,
                backgroundColor: backgroundColor
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
    });
         $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        url: "http://localhost/got8/index.php/statsville",
        success: function(data){
            nombreVille = JSON.parse(data);
        }

        });
    $('#btn-stats-ville').click(function(){
        
        var nb = new Array();
        var ville = new Array();
        var backgroundColor = new Array();
        for (var i = 0; i < nombreVille.length; i++) {
          nb[i] = nombreVille[i].nb;
          ville[i] = nombreVille[i].name;
          backgroundColor[i] = getRandomColor();
        }
        var ctx = document.getElementById("statsVille").getContext('2d');
        var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ville,
            datasets: [{
                label: 'Nombre de ville de type',
                data: nb,
                backgroundColor: backgroundColor
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
    });
    $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        url: "http://localhost/got8/index.php/tranchePerso",
        success: function(data){
            tranchePerso = JSON.parse(data);
        }

        });
    $('#btn-stats-perso').click(function(){
        
        var nb = new Array();
        var backgroundColor = new Array();
        for (var i = 0; i < tranchePerso.length; i++) {
          nb[i] = tranchePerso[i].nb;
          backgroundColor[i] = getRandomColor();
        }
        var ctx = document.getElementById("statsPerso").getContext('2d');
        var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['-50 à 0', '1 à 50', '51 à 100', '101 à 150', '151 à 200', '201 à 250', '251 à 300'],
            datasets: [{
                label: 'Nombre de perso par tranche de 50 ans',
                data: nb,
                backgroundColor: backgroundColor
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
    });
    $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        url: "http://localhost/got8/index.php/statsCultures",
        success: function(data){
            cultures = JSON.parse(data);
        }

        });
    $('#btn-stats-cult').click(function(){

        var nb = new Array();
        var backgroundColor = new Array();
        var nomCulture = new Array();
        for (var i = 0; i < cultures.length; i++) {
          nb[i] = cultures[i].nb;
          nomCulture[i] = cultures[i].cult;
          backgroundColor[i] = getRandomColor();
                  }
        var ctx = document.getElementById("stats").getContext('2d');
        var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: nomCulture,
            datasets: [{
                label: 'Nombre de perso par culture',
                data: nb,
                backgroundColor: backgroundColor
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
});
    $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        url: "http://localhost/got8/index.php/nbPersoByLord",
        success: function(data){
            lordName = JSON.parse(data);
        }

        });
    $('#btn-stats-lord').click(function(){

        var nb = new Array();
        var backgroundColor = new Array();
        var nomLord = new Array();
        for (var i = 0; i < lordName.length; i++) {
          nb[i] = lordName[i].nb;
          nomLord[i] = lordName[i].name;
          backgroundColor[i] = getRandomColor();
                  }
        var ctx = document.getElementById("stats").getContext('2d');
        var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: nomLord,
            datasets: [{
                label: 'Nombre de perso par seigneur',
                data: nb,
                backgroundColor: backgroundColor
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
});
    $("#btn-lord").click(function(event) {
  $("#tableau_utilisateur").bootstrapTable("destroy");
  $("#tableau_utilisateur").bootstrapTable({
url: "http://localhost/got8/index.php/nbPersoByLord",
columns: [{
field: "nb",
title: 'Culture'
}, {
field: 'name',
title: 'Nom'
},
]
});

});
$.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        url: "http://localhost/got8/index.php/housesOrderByDate",
        success: function(data){
            housesOrder = JSON.parse(data);
        }

        });
    $('#btn-housesOrder').click(function(){

        var date = new Array();
        var backgroundColor = new Array();
        var nameHouses = new Array();
        for (var i = 0; i < housesOrder.length; i++) {
          date[i] = housesOrder[i].founded;
          nameHouses[i] = housesOrder[i].name;
          backgroundColor[i] = getRandomColor();
                  }
        var ctx = document.getElementById("stats").getContext('2d');
        var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: nameHouses,
            datasets: [{
                label: 'Maison par date de création',
                data: date,
                borderColor: backgroundColor
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
});
});
