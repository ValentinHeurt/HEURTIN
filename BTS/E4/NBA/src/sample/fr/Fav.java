package sample.fr;

public class Fav {
    private int id_user;
    private int id_team;
    public Fav(int id_team, int id_user){
        this.id_user = id_user;
        this.id_team = id_team;
    }
    public Fav(){}

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_team() {
        return id_team;
    }

    public void setId_team(int id_team) {
        this.id_team = id_team;
    }
}
