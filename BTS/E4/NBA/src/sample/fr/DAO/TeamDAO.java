package sample.fr.DAO;

import sample.fr.DAO.DAO;
import sample.fr.Team;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class TeamDAO extends DAO {
    public Team getTeamByID(int id){
        Team team= null;
        Connection conn;
        try{
            conn = super.getConnexion();
            String sql = "SELECT * FROM team WHERE id_team = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1,id);
            ResultSet result = pstmt.executeQuery();
            team = new Team();
            while(result.next()) {
                team.setId(result.getInt(1));
                team.setNom(result.getString(2));
                team.setConf(result.getString(3));
                team.setVictoire(result.getInt(4));
                team.setDefaite(result.getInt(5));
                team.setNick(result.getString(6));
                team.setLogo(result.getString(7));

            }
            pstmt.close();
            conn.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return team;

    }
    public Team getTeamByNick(String nick){
        Team team= null;
        Connection conn;
        try{
            conn = super.getConnexion();
            String sql = "SELECT * FROM team WHERE nick = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1,nick);
            ResultSet result = pstmt.executeQuery();
            team = new Team();
            while(result.next()) {
                team.setId(result.getInt(1));
                team.setNom(result.getString(2));
                team.setConf(result.getString(3));
                team.setVictoire(result.getInt(4));
                team.setDefaite(result.getInt(5));
                team.setNick(result.getString(6));
                team.setLogo(result.getString(7));

            }
            pstmt.close();
            conn.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return team;

    }
    public ArrayList<Team> getAllTeam(){
        ArrayList<Team> listTeam = new ArrayList<Team>();
        Connection conn;
        try{
            conn = super.getConnexion();
            String sql = "SELECT * FROM team";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                listTeam.add(new Team(result.getInt(1),result.getString(2),result.getString(3),result.getInt(4),result.getInt(5),result.getString(6),result.getString(7)));
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return listTeam;
    }

    public void win(int victoire, int id){
        Connection conn;
        try{
            conn = super.getConnexion();
            String sql = "UPDATE team SET victoireSaison = ? WHERE id_team = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1,victoire);
            pstmt.setInt(2,id);
            pstmt.executeUpdate();
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void lose(int defaite, int id){
        Connection conn;
        try{
            conn = super.getConnexion();
            String sql = "UPDATE team SET defaiteSaison = ? WHERE id_team = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1,defaite);
            pstmt.setInt(2,id);
            pstmt.executeUpdate();
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    }
