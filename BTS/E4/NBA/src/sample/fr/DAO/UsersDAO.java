package sample.fr.DAO;
import com.sun.org.apache.regexp.internal.RE;
import sample.fr.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UsersDAO extends DAO {
    public User getUserByID(int id){
        User user= null;
        Connection conn;
        try{
            conn = super.getConnexion();
            String sql = "SELECT * FROM users WHERE id = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1,id);
            ResultSet result = pstmt.executeQuery();
            user = new User();
            while(result.next()) {
                user.setId(result.getInt(1));
                user.setPseudo(result.getString(2));
                user.setGrade(result.getInt(4));
                user.setPhoto(result.getString(5));
                user.setTimestamp(result.getTimestamp(7));


            }
            pstmt.close();
            conn.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return user;

    }
    public User log(String login, String pass){
        User user= null;
        Connection conn;
        try{
            conn = super.getConnexion();
            String sql = "SELECT * FROM users WHERE pseudo = ? AND mdp = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1,login);
            pstmt.setString(2,pass);
            ResultSet result = pstmt.executeQuery();


            while(result.next()) {
                user = new User();
                user.setId(result.getInt(1));
                user.setPseudo(result.getString(2));
                user.setGrade(result.getInt(4));
                user.setPhoto(result.getString(5));
                user.setEmail(result.getString(6));
                user.setTimestamp(result.getTimestamp(7));


            }
            pstmt.close();
            conn.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return user;
    }
    public boolean checkPseudo(String pseudo){
        Connection conn;
        boolean test = true;
        try{
            conn = super.getConnexion();
            String sql = "SELECT pseudo FROM users";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            ResultSet resultat = pstmt.executeQuery();
            while (resultat.next()){
                if(pseudo.equals(resultat.getString("pseudo"))){
                    test = false;
                }
            }
        }catch(Exception e){
        e.printStackTrace();
    }
        return test;
    }
    public void register(String pseudo,String email, String pass){
        Connection conn;
        try{
            conn = super.getConnexion();
            String sql = "INSERT INTO users (pseudo,mdp,email) VALUES(?,?,?)";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1,pseudo);
            pstmt.setString(2,pass);
            pstmt.setString(3,email);
            pstmt.executeUpdate();

            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void setMdp(String pseudo, String mdp){
        Connection conn ;
        try{
            conn = super.getConnexion();
            String sql ="UPDATE users SET mdp = ? WHERE pseudo = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1,mdp);
            pstmt.setString(2,pseudo);
            pstmt.executeUpdate();

            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void setPhoto(String pseudo, String photo){
        Connection conn ;
        try{
            conn = super.getConnexion();
            String sql ="UPDATE users SET photo = ? WHERE pseudo = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1,photo);
            pstmt.setString(2,pseudo);
            pstmt.executeUpdate();

            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void deco(String pseudo){
        Connection conn;
        try{
            conn = super.getConnexion();
            String sql = "UPDATE users SET dateDeco = NOW() WHERE pseudo = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1,pseudo);
            pstmt.executeUpdate();
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }

    }


}
