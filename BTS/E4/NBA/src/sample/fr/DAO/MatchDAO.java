package sample.fr.DAO;
import sample.fr.Match;
import sample.fr.Team;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class MatchDAO extends DAO {
    public Match getMatchByID(int id){
        Connection conn;
        Match match = null;
        try{
            conn = getConnexion();
            match = new Match();
            TeamDAO tdao = new TeamDAO();
            String sql = "SELECT * FROM match WHERE id = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1,id);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                match.setId(result.getInt(1));
                match.setTeam1(tdao.getTeamByID(result.getInt(2)));
                match.setTeam2(tdao.getTeamByID(result.getInt(3)));
                match.setScoreTeam1(result.getInt(4));
                match.setScoreTeam2(result.getInt(5));
                match.setDateAjout(result.getTimestamp(6));
                pstmt.close();
                conn.close();

            }



        }catch (Exception e){
            e.printStackTrace();
        }
        return match;
    }
    public void newMatch(Team team1, Team team2, int scoreTeam1, int scoreTeam2){
        Connection conn;
        try{
            String sql = "INSERT INTO `match` (team1,team2,scoreTeam1,scoreTeam2,dateAjout) VALUES (?,?,?,?,NOW())";
            conn = super.getConnexion();
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, team1.getId());
            pstmt.setInt(2,team2.getId());
            pstmt.setInt(3,scoreTeam1);
            pstmt.setInt(4,scoreTeam2);
            pstmt.executeUpdate();
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }


    }
    public ArrayList<Match> getAllMatchByTeam(Team team){
        Connection conn;
        ArrayList<Match> matchs = new ArrayList<Match>();
        TeamDAO dao = new TeamDAO();
        try{
            String sql = "SELECT * FROM `match` WHERE team1 = ? OR team2 = ?";
            conn = super.getConnexion();
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1,team.getId());
            pstmt.setInt(2,team.getId());
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                matchs.add(new Match(result.getInt(1),dao.getTeamByID(result.getInt(2)),dao.getTeamByID(result.getInt(3)),result.getInt(4),result.getInt(5),result.getTimestamp(6)));
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return matchs;
    }
    public ArrayList<Match> getAllMatchByFav(int id){
        ArrayList<Match> favMatch = new ArrayList<Match>();
        Connection conn;
        TeamDAO dao = new TeamDAO();
        try{
            conn = super.getConnexion();
            String sql = "SELECT `match`.id,team1,team2,scoreTeam1,scoreTeam2,dateAjout FROM `match`,team,fav,users WHERE fav.id_team = team.id_team AND fav.id_user = ? AND (team1 = team.id_team OR team2 = team.id_team) GROUP BY `match`.id";
            conn = super.getConnexion();
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1,id);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                favMatch.add(new Match(result.getInt(1),dao.getTeamByID(result.getInt(2)),dao.getTeamByID(result.getInt(3)),result.getInt(4),result.getInt(5),result.getTimestamp(6)));
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return favMatch;
    }
}
