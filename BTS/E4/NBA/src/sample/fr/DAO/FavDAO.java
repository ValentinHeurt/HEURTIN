package sample.fr.DAO;
import sample.fr.Fav;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class FavDAO extends DAO {
    public ArrayList<Fav> getFavByID(int id){
        Connection conn;

        ArrayList<Fav> fav = new ArrayList<Fav>();
        try{
            conn = getConnexion();

            TeamDAO tdao = new TeamDAO();
            String sql = "SELECT * FROM fav WHERE id_user = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1,id);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                fav.add(new Fav(result.getInt(1),result.getInt(2)));

            }
            pstmt.close();
            conn.close();



        }catch (Exception e){
            e.printStackTrace();
        }
        return fav;
    }
    public void newFav(Fav fav){
        Connection conn;
        try{
            String sql = "INSERT INTO fav VALUES (?,?)";
            conn = super.getConnexion();
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, fav.getId_team());
            pstmt.setInt(2,fav.getId_user());
            pstmt.executeUpdate();
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }


    }
    public void delFav(Fav fav){
        Connection conn;
        try{
            String sql = "DELETE FROM fav WHERE id_user = ? AND id_team = ?";
            conn = super.getConnexion();
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1,fav.getId_user());
            pstmt.setInt(2,fav.getId_team());
            pstmt.executeUpdate();
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
