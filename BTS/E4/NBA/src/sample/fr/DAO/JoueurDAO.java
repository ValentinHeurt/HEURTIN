package sample.fr.DAO;
import sample.fr.Joueur;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class JoueurDAO extends DAO {
    public Joueur getJoueurByID(int id){
        Joueur joueur=null;
        Connection conn;
        try{
            conn = super.getConnexion();
            String sql = "SELECT * FROM joueur WHERE id = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1,id);
            ResultSet result = pstmt.executeQuery();
            joueur = new Joueur();
            TeamDAO tdao = new TeamDAO();

            while (result.next()) {
                joueur.setId(result.getInt(1));
                joueur.setNom(result.getString(2));
                joueur.setPrenom((result.getString(3)));
                joueur.setAge(result.getInt(4));
                joueur.setTeam(tdao.getTeamByID(result.getInt(5)));
                joueur.setTotalPoints(result.getInt(6));
                joueur.setTotalMin(result.getInt(7));
                joueur.setMoyMin(result.getFloat(8));
                joueur.setMoyPts(result.getFloat(9));
                joueur.setPcTirs(result.getFloat(10));
                joueur.setPc3pt(result.getFloat(11));
                joueur.setMoyReb(result.getFloat(12));
                joueur.setMoyAst(result.getFloat(13));
                joueur.setTotalAst(result.getInt(14));
                joueur.setTotalReb(result.getInt(15));
                joueur.setMoyStl(result.getFloat(16));
                joueur.setTotalStl(result.getInt(17));
                joueur.setMoyBLK(result.getFloat(18));
                joueur.setTotalBLK(result.getInt(19));
                joueur.setMoyPF(result.getFloat(20));
                joueur.setTotalPF(result.getInt(21));




            }
            pstmt.close();
            conn.close();

        }catch(Exception e){
            e.printStackTrace();
        }
        return joueur;


    }
    public Joueur getJoueurByNom(String nom){
        Joueur joueur = null;
        Connection conn;
        TeamDAO tdao = new TeamDAO();
        try{
            conn = super.getConnexion();
            String sql = "SELECT * FROM joueurs WHERE nom = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1,nom);
            ResultSet result = pstmt.executeQuery();
            joueur = new Joueur();
            while(result.next()) {
                joueur.setId(result.getInt(1));
                joueur.setNom(result.getString(2));
                joueur.setPrenom((result.getString(3)));
                joueur.setAge(result.getInt(4));
                joueur.setTeam(tdao.getTeamByID(result.getInt(5)));
                joueur.setTotalPoints(result.getInt(6));
                joueur.setTotalMin(result.getInt(7));
                joueur.setMoyMin(result.getFloat(8));
                joueur.setMoyPts(result.getFloat(9));
                joueur.setPcTirs(result.getFloat(10));
                joueur.setPc3pt(result.getFloat(11));
                joueur.setMoyReb(result.getFloat(12));
                joueur.setMoyAst(result.getFloat(13));
                joueur.setTotalAst(result.getInt(14));
                joueur.setTotalReb(result.getInt(15));
                joueur.setMoyStl(result.getFloat(16));
                joueur.setTotalStl(result.getInt(17));
                joueur.setMoyBLK(result.getFloat(18));
                joueur.setTotalBLK(result.getInt(19));
                joueur.setMoyPF(result.getFloat(20));
                joueur.setTotalPF(result.getInt(21));


            }
            pstmt.close();
            conn.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return joueur;

    }
}
