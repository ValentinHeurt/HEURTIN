package sample.fr;

import java.sql.Timestamp;

public class Match {
    private int id;
    private Team team1;
    private Team team2;
    private int scoreTeam1;
    private int scoreTeam2;
    private Timestamp dateAjout;


    @Override
    public String toString() {
        return "Match{" +
                "id=" + id +
                ", team1=" + team1 +
                ", team2=" + team2 +
                ", scoreTeam1=" + scoreTeam1 +
                ", scoreTeam2=" + scoreTeam2 +
                '}';
    }


    public Match(int id, Team team1, Team team2, int scoreTeam1, int scoreTeam2, Timestamp dateAjout){
        this.id = id;
        this.team1 = team1;
        this.team2 = team2;
        this.scoreTeam1 = scoreTeam1;
        this.scoreTeam2 = scoreTeam2;
        this.dateAjout = dateAjout;


    }
    public Match(){}

    public Timestamp getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Timestamp dateAjout) {
        this.dateAjout = dateAjout;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Team getTeam1() {
        return team1;
    }

    public void setTeam1(Team team1) {
        this.team1 = team1;
    }

    public Team getTeam2() {
        return team2;
    }

    public void setTeam2(Team team2) {
        this.team2 = team2;
    }

    public int getScoreTeam1(){return scoreTeam1; }

    public void setScoreTeam1(int scoreTeam1){ this.scoreTeam1 = scoreTeam1; }

    public int getScoreTeam2(){ return scoreTeam2; }

    public void setScoreTeam2(int scoreTeam2){ this.scoreTeam2 = scoreTeam2; }
}
