package sample.fr.pane;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import sample.fr.*;
import sample.fr.DAO.TeamDAO;
import sample.fr.DAO.UsersDAO;

import java.util.ArrayList;

public class AccueilUserPane extends GridPane {



    public AccueilUserPane(){
        TeamDAO dao = new TeamDAO();
        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25,25,25,25));
        this.setId("AccueilUserPane");
        Text test = new Text("Accueil");
        this.add(test,0,0);
        ArrayList<Team> teams = dao.getAllTeam();
        ArrayList<HBox> bs = new ArrayList<HBox>();
        int taille = teams.size();
        for(int i = 0 ; i<taille; i++){
            bs.add(new HBox());

        }
        int compOuest = 0;
        int compEst = 0;

        for(int i = 0; i<teams.size();i++){
            VBox v = new VBox();
            HBox b2 = new HBox();
            bs.get(i).setId("teams");

            bs.get(i).getChildren().add(v);
            bs.get(i).getChildren().add(b2);
            Team teamTemp = teams.get(i);
            Image imgTeam = new Image(teamTemp.getLogo(), 50, 50, false, false);
            ImageView imgvTeam = new ImageView(imgTeam);
            v.getChildren().add(new Text(teamTemp.getNom()));
            v.getChildren().add(new Text("Victoires : "+teamTemp.getVictoire()+" / Defaites : "+teamTemp.getDefaite()+"  "));
            b2.getChildren().add(imgvTeam);
            bs.get(i).setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    TeamPane tPane = new TeamPane();
                    tPane.setTeam(teamTemp);
                    tPane.initialisation();
                    Main.getScene().setRoot(tPane);



                }
            });

            if(teams.get(i).getConf().equals("est")) {
                this.add(bs.get(i),1,compEst+1);
                compEst = compEst + 1 ;
            }else {
                this.add(bs.get(i),0,compOuest+1);
                compOuest = compOuest + 1;
            }


            }
        Button rechJ = new Button("Recharcher un joueur");
        rechJ.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                RechercheJoueurPane rPane = new RechercheJoueurPane();
                Main.getScene().setRoot(rPane);
            }
        });
        this.add(rechJ,0,compOuest+1);
        Button fav = new Button("favori");
        fav.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                favPane fPane = new favPane();
                fPane.initialisation();
                Main.getScene().setRoot(fPane);
            }
        });
        this.add(fav,0,compOuest+2);




        };



    public void initialisation(){
        HBox hb = new HBox();
        hb.setAlignment(Pos.CENTER_RIGHT);
        if(Main.u.getGrade() == 0){
            Button btnUser = new Button("Profil");
            btnUser.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    ProfilPane pPane = new ProfilPane();
                    Main.getScene().setRoot(pPane);
                }
            });
            hb.getChildren().add(btnUser);

        }
        if(Main.u.getGrade() == 1){
            Button btnAdmin = new Button("options");
            btnAdmin.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    OptionPane oPane = new OptionPane();
                    Main.getScene().setRoot(oPane);

                }
            });
            hb.getChildren().add(btnAdmin);
        }
        Button deco = new Button("Deconnexion");
        deco.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                UsersDAO dao = new UsersDAO();
                dao.deco(Main.u.getPseudo());

                Main.u = null;
                loginPane lPane = new loginPane();
                Main.getScene().setRoot(lPane);
            }
        });
        hb.getChildren().add(deco);
        this.add(hb,1,0);



    }}

