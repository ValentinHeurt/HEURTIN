package sample.fr.pane;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import sample.fr.DAO.FavDAO;
import sample.fr.DAO.MatchDAO;
import sample.fr.Fav;
import sample.fr.Main;
import sample.fr.Match;
import sample.fr.Team;


import java.util.ArrayList;

public class TeamPane extends GridPane {
    private Team team;
    public TeamPane(){
        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25,25,25,25));
        this.setId("TeamPane");



    }
    public void setTeam(Team team){
        this.team = team;
    }
    public void initialisation(){
        MatchDAO dao = new MatchDAO();
        ArrayList<Match> matchs = dao.getAllMatchByTeam(team);
        ArrayList<HBox> hs = new ArrayList<HBox>();
        int taille = matchs.size();
        for(int i = 0 ; i<taille; i++){
            hs.add(new HBox());

        }
        int compDirection = 0;
        int compG = 0;
        int compD = 0;

        for(int i = 0 ; i<taille ; i++){
            Image imgLogo1 = new Image(matchs.get(i).getTeam1().getLogo(),50,50,false,false);
            ImageView imgvLogo1 = new ImageView(imgLogo1);
            Image imgLogo2 = new Image(matchs.get(i).getTeam2().getLogo(),50,50,false,false);
            ImageView imgvLogo2 = new ImageView(imgLogo2);
            Text pts1 = new Text(""+matchs.get(i).getScoreTeam1());
            Text space = new Text(" - ");
            Text pts2 = new Text(""+matchs.get(i).getScoreTeam2());
            hs.get(i).getChildren().add(imgvLogo1);
            hs.get(i).getChildren().add(pts1);
            hs.get(i).getChildren().add(space);
            hs.get(i).getChildren().add(pts2);
            hs.get(i).getChildren().add(imgvLogo2);
            if(Main.u.getTimestamp().compareTo(matchs.get(i).getDateAjout())<0){
                Text nouveau = new Text("New");
                nouveau.setFill(Color.GREEN);
                hs.get(i).getChildren().add(nouveau);
            }
            hs.get(i).setId("match");
            if(compDirection == 0){
                this.add(hs.get(i),0,compG+1);
                compG = compG+1;
                compDirection = 1;

            }else{
                this.add(hs.get(i),1,compD+1);
                compD = compD+1;
                compDirection = 0;
            }
        }
        FavDAO fdao = new FavDAO();
        Button btn = new Button("Retour");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                AccueilUserPane aPane = new AccueilUserPane();
                aPane.initialisation();
                Main.getScene().setRoot(aPane);

            }
        });
        Button fav = new Button("fav");
        Button paFav = new Button("pas fav");
        Text favoupa = new Text();
        fav.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                fdao.newFav(new Fav(team.getId(),Main.u.getId()));
                favoupa.setText("Favori");

            }
        });
        paFav.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                fdao.delFav(new Fav(team.getId(),Main.u.getId()));
                favoupa.setText("Pas favori");
            }
        });
        ArrayList<Fav> fArray = fdao.getFavByID(Main.u.getId());
        int test = 0;
        for(int i = 0 ; i<fArray.size() ; i++){
            if(fArray.get(i).getId_team() == team.getId()){
                favoupa.setText("Favori");
            }else{
                test = test +1;
            }
        }
        if(test == fArray.size()){
            favoupa.setText("Pas favori");
        }
        this.add(btn,0,compG+1);
        this.add(fav,1,compG+1);
        this.add(paFav,2,compG+1);
        this.add(favoupa,3,compG+1);
    }

}
