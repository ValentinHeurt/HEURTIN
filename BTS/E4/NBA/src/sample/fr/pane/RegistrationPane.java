package sample.fr.pane;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import sample.fr.DAO.UsersDAO;
import sample.fr.Main;


public class RegistrationPane extends GridPane{
    private PasswordField pwBox = new PasswordField();
    private TextField userTextField = new TextField();
    public RegistrationPane(){
        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25,25,25,25));
        UsersDAO dao = new UsersDAO();
        Text scenetitle = new Text("Connexion");
        Label pseudo = new Label("Pseudo : ");
        Label email = new Label("Email : ");
        Label mdp = new Label("Mot de passe : ");
        Label confirmationMdp = new Label("Confirmation du mot de passe : ");

        this.add(scenetitle,3,0);
        this.add(pseudo,3,1);
        this.add(email,3,2);
        this.add(mdp,3,3);
        this.add(confirmationMdp,3,4);

        PasswordField pwBox = new PasswordField();
        PasswordField confirmePwBox = new PasswordField();
        TextField pseudoField = new TextField();
        TextField emailField = new TextField();

        this.add(pseudoField, 4, 1);
        this.add(emailField,4,2);
        this.add(pwBox,4,3);
        this.add(confirmePwBox,4,4);

        Button register = new Button("Inscription");
        this.add(register,4,5);
        Text rep = new Text();
        this.add(rep,4,6);
        Button retour = new Button("Retour à la page de connexion");
        this.add(retour,4,7);
        retour.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                loginPane lPane = new loginPane();
                Main.getScene().setRoot(lPane);
            }
        });



        register.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String sonPseudo = pseudoField.getText();
                String sonEmail = emailField.getText();
                String mdp = pwBox.getText();
                String confirmationMdp = confirmePwBox.getText();
                if(confirmationMdp.equals(mdp) && dao.checkPseudo(sonPseudo)){
                    dao.register(sonPseudo,sonEmail,mdp);
                    rep.setText("Inscription réussite");
                    rep.setFill(Color.GREEN);

                }else {
                    if(!confirmationMdp.equals(mdp) && dao.checkPseudo(sonPseudo)){
                        rep.setText("La confirmation de mot de passe est différente du mot de passe");
                        rep.setFill(Color.RED);

                    }
                    if(confirmationMdp.equals(mdp) && !dao.checkPseudo(sonPseudo)){
                        rep.setText("Ce pseudo est déjà utilisé");
                        rep.setFill(Color.RED);
                    }
                    if(!confirmationMdp.equals(mdp) && !dao.checkPseudo(sonPseudo)){
                        rep.setText("La confirmation de mot de passe est différente du mot de passe et ce pseudo esst déjà utilisé");
                        rep.setFill(Color.RED);

                    }
                }

            }
        });






    }
}
