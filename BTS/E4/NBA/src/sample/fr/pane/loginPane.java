package sample.fr.pane;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import sample.fr.DAO.UsersDAO;
import sample.fr.Main;
import sample.fr.User;

public class loginPane extends GridPane {

    private PasswordField pwBox = new PasswordField();
    private TextField userTextField = new TextField();
    public loginPane(){
        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25,25,25,25));
        this.setId("loginPane");
        UsersDAO dao = new UsersDAO();
        Text scenetitle = new Text("Connexion");
        Text pseudo = new Text("Pseudo : ");
        Text mdp = new Text("Mots de passe : ");
        scenetitle.setFill(Color.WHITE);
        pseudo.setFill(Color.WHITE);
        mdp.setFill(Color.WHITE);

        this.add(scenetitle,3,0);
        this.add(pseudo,3,1);
        this.add(mdp,3,2);
        this.add(userTextField,4,1);
        this.add(pwBox,4,2);

        Button log = new Button("Connexion");
        Text info = new Text("Vous n'avez pas de compte ?");
        Button inscription = new Button("S'inscrire");

        this.add(log,4,3);
        this.add(info,3,4);
        this.add(inscription,4,4);
        Text actiontarget = new Text();
        this.add(actiontarget, 3, 5);
        Text failtoco = new Text("");
        this.add(failtoco,3,6);

        log.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String login = userTextField.getText();
                String pwd = pwBox.getText();
                Main.u = dao.log(login,pwd);
                if(Main.u != null){
                    AccueilUserPane aPane = new AccueilUserPane();
                    aPane.initialisation();
                    Main.getScene().setRoot(aPane);





            }
            else{
                    failtoco.setText("Ce compte n'existe pas");
                }
            }
        });
        inscription.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                RegistrationPane rPane = new RegistrationPane();
                Main.getScene().setRoot(rPane);
            }
        });





    }

}
