package sample.fr.pane;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import sample.fr.DAO.MatchDAO;
import sample.fr.DAO.TeamDAO;
import sample.fr.Main;
import sample.fr.Team;

import java.util.ArrayList;

public class OptionPane extends GridPane {
    public OptionPane(){
        TeamDAO dao = new TeamDAO();
        MatchDAO mDao = new MatchDAO();
        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25,25,25,25));
        this.setId("OptionPane");
        this.add(new Text("Options"),0,0);
        this.add(new Text("Ajout de match :"),0,1);


        ArrayList<Team> lesteams = dao.getAllTeam();
        ComboBox<String> teams1 = new ComboBox<String>();
        teams1.getItems().add(0,"Equipe 1");
        for(int i = 0 ; i<lesteams.size() ; i++){
            teams1.getItems().add(i+1,lesteams.get(i).getNick());
        }
        teams1.getSelectionModel().select(0);
        this.add(teams1,1,1);
        Label scoreLabel1 = new Label("Score :");
        this.add(scoreLabel1,2,1);

        TextField score1 = new TextField();
        this.add(score1,3,1);

        ComboBox<String> teams2 = new ComboBox<String>();
        teams2.getItems().add(0,"Equipe 2");
        for(int i = 0 ; i<lesteams.size() ; i++){
            teams2.getItems().add(i+1,lesteams.get(i).getNick());
        }
        teams2.getSelectionModel().select(0);
        this.add(teams2,4,1);
        Label scoreLabel2 = new Label("Score :");
        this.add(scoreLabel2,5,1);
        TextField score2 = new TextField();
        this.add(score2,6,1);
        Text result = new Text();
        this.add(result,0,3);

        Button add = new Button("Ajouter");
        add.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(dao.getTeamByNick(teams1.getValue()).getNick().equals(dao.getTeamByNick(teams2.getValue()).getNick())){
                    result.setText("Ajout refusé, les deux équipes sont identiques");
                    result.setFill(Color.RED);
                }else {
                    mDao.newMatch(dao.getTeamByNick(teams1.getValue()), dao.getTeamByNick(teams2.getValue()), Integer.parseInt(score1.getText()), Integer.parseInt(score2.getText()));
                    if (Integer.parseInt(score1.getText()) < Integer.parseInt(score2.getText())) {
                        dao.win(dao.getTeamByNick(teams2.getValue()).getVictoire() + 1, dao.getTeamByNick(teams2.getValue()).getId());
                        dao.lose(dao.getTeamByNick(teams1.getValue()).getDefaite() + 1, dao.getTeamByNick(teams1.getValue()).getId());
                    }
                    if (Integer.parseInt(score2.getText()) < Integer.parseInt(score1.getText())) {
                        dao.win(dao.getTeamByNick(teams1.getValue()).getVictoire() + 1, dao.getTeamByNick(teams1.getValue()).getId());
                        dao.lose(dao.getTeamByNick(teams2.getValue()).getDefaite() + 1, dao.getTeamByNick(teams2.getValue()).getId());
                    }
                    result.setText("Ajout réussi");
                    result.setFill(Color.GREEN);
                }
            }
        });
        Button retour = new Button("Retour");
        retour.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                AccueilUserPane aPane = new AccueilUserPane();
                aPane.initialisation();
                Main.getScene().setRoot(aPane);

            }
        });
        this.add(add,7,1);
        this.add(retour,0,2);

    }
}
