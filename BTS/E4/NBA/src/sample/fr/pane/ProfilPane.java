package sample.fr.pane;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import sample.fr.DAO.UsersDAO;
import sample.fr.Main;
import sample.fr.User;

import java.util.Optional;

public class ProfilPane extends GridPane {
    public ProfilPane(){
        UsersDAO dao = new UsersDAO();
        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25,25,25,25));
        this.setId("ProfilPane");
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25,25,25,25));
        this.add(grid,0,0);
        grid.setId("grideu");

        grid.add(new Text("Profil"),0,0);

        grid.add(new Text("Pseudo :"),0,2);
        User user = Main.u;
        StackPane sp = new StackPane();
        Image img = new Image(user.getPhoto(),120,120,false,false);
        ImageView imgv = new ImageView(img);
        sp.getChildren().add(imgv);
        grid.add(imgv,1,0);

        imgv.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                TextInputDialog dialog = new TextInputDialog("url nouvelle image de profil");
                dialog.setTitle("modification image");
                dialog.setHeaderText("Entrez le lien d'une nouvelle image");

                Optional<String> result = dialog.showAndWait();
                if (result.isPresent()){
                    dao.setPhoto(user.getPseudo(),result.get());
                    Main.u.setPhoto(result.get());
                    Image imgNew = new Image(user.getPhoto(),120,120,false,false);
                    imgv.setImage(imgNew);
                }
            }
        });


        grid.add(new Text(user.getPseudo()),1,2);


        grid.add(new Text("Email :"),0,3);
        grid.add(new Text(user.getEmail()),1,3);

        grid.add(new Text("Modifier le mot de passe :"),0,4);
        PasswordField oldMdp = new PasswordField();
        oldMdp.setPromptText("Ancien mot de passe");
        grid.add(oldMdp,0,5);

        PasswordField newMdp = new PasswordField();
        newMdp.setPromptText("Nouveau mot de passe");
        grid.add(newMdp,0,6);

        PasswordField confNewMdp = new PasswordField();
        confNewMdp.setPromptText("Confirmation du nouveau mot de passe");
        grid.add(confNewMdp,0,7);
        Text rep = new Text();
        grid.add(rep,0,8);

        Button modifMdp = new Button("modifier");
        modifMdp.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String ancien = oldMdp.getText();
                String nouveau = newMdp.getText();
                String confNouveau = confNewMdp.getText();
                if(nouveau.equals(confNouveau) && dao.log(user.getPseudo(),ancien) != null){
                    dao.setMdp(user.getPseudo(),nouveau);
                    rep.setText("Modification réussite");
                    rep.setFill(Color.GREEN);

                }else{
                    rep.setText("Erreur");
                    rep.setFill(Color.RED);
                }
            }
        });
        grid.add(modifMdp,1, 7);
        Button btn = new Button("Retour");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                AccueilUserPane aPane = new AccueilUserPane();
                aPane.initialisation();
                Main.getScene().setRoot(aPane);

            }
        });
        this.add(btn,0,1);










    }
}
