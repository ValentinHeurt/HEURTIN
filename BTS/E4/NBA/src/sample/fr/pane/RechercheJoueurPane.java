package sample.fr.pane;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import sample.fr.DAO.JoueurDAO;
import sample.fr.Joueur;
import sample.fr.Main;

public class RechercheJoueurPane extends GridPane {
    public RechercheJoueurPane(){
        JoueurDAO dao = new JoueurDAO();
        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25,25,25,25));
        this.setId("RechercheJoueurPane");
        Text rechercher = new Text("Rechercher");
        this.add(rechercher,0,0);
        TextField rech = new TextField();
        this.add(rech,1,0);
        Text rep = new Text();
        this.add(rep,0,2);
        Button go = new Button("chercher");
        Text nom =  new Text();
        Text nomA = new Text();
        Text prenom = new Text();
        Text prenomA = new Text();
        Text team = new Text();
        Text teamA = new Text();

        go.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Joueur j = dao.getJoueurByNom(rech.getText());
                if(j == null){
                    rep.setText("Ce joueur n'existe pas");
                    rep.setFill(Color.RED);
                }else{
                    nom.setText(j.getNom());
                    nomA.setText("Nom :");
                    prenomA.setText("Prenom :");
                    prenom.setText(j.getPrenom());
                    teamA.setText("Equipe :");
                    team.setText(j.getTeam().getNom());


                }


            }
        });
        this.add(nomA,0,2);
        this.add(nom,1,2);
        this.add(prenomA,0,3);
        this.add(prenom,1,3);
        this.add(team,1,4);
        this.add(teamA,0,4);

        this.add(go,0,1);
        Button retour = new Button("Retour");
        retour.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                AccueilUserPane aPane = new AccueilUserPane();
                aPane.initialisation();
                Main.getScene().setRoot(aPane);

            }
        });
        this.add(retour,0,5);
    }
}
