package sample.fr.pane;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import sample.fr.DAO.FavDAO;
import sample.fr.DAO.MatchDAO;
import sample.fr.DAO.TeamDAO;
import sample.fr.Fav;
import sample.fr.Main;
import sample.fr.Match;
import sample.fr.Team;


import java.util.ArrayList;

public class favPane extends GridPane {
    private Team team;
    public favPane(){
        this.setAlignment(Pos.CENTER);
        this.setHgap(20);
        this.setVgap(20);
        this.setPadding(new Insets(25,25,25,25));
        this.setId("TeamPane");



    }
    public void initialisation(){
        MatchDAO dao = new MatchDAO();
        FavDAO fdao = new FavDAO();
        TeamDAO tdao = new TeamDAO();
        ArrayList<Match> matchs = dao.getAllMatchByFav(Main.u.getId());
        ArrayList<HBox> hs = new ArrayList<HBox>();


        int compDirection = 0;
        int compG = 0;
        int compD = 0;
        int taille = matchs.size();;
        for(int i = 0 ; i<taille; i++){
            hs.add(new HBox());

        }

        for(int i = 0 ; i<taille ; i++){
            Image imgLogo1 = new Image(matchs.get(i).getTeam1().getLogo(),50,50,false,false);
            ImageView imgvLogo1 = new ImageView(imgLogo1);
            Image imgLogo2 = new Image(matchs.get(i).getTeam2().getLogo(),50,50,false,false);
            ImageView imgvLogo2 = new ImageView(imgLogo2);
            Text pts1 = new Text(""+matchs.get(i).getScoreTeam1());
            Text space = new Text(" - ");
            Text pts2 = new Text(""+matchs.get(i).getScoreTeam2());
            hs.get(i).getChildren().add(imgvLogo1);
            hs.get(i).getChildren().add(pts1);
            hs.get(i).getChildren().add(space);
            hs.get(i).getChildren().add(pts2);
            hs.get(i).getChildren().add(imgvLogo2);
            hs.get(i).setId("match");
            if(compDirection == 0){
                this.add(hs.get(i),0,compG+1);
                compG = compG+1;
                compDirection = 1;

            }else{
                this.add(hs.get(i),1,compD+1);
                compD = compD+1;
                compDirection = 0;
            }
        }
        Button btn = new Button("Retour");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                AccueilUserPane aPane = new AccueilUserPane();
                aPane.initialisation();
                Main.getScene().setRoot(aPane);

            }
        });
        this.add(btn,0,compG+1);
    }

}
