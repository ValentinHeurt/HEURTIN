package sample.fr;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import sample.fr.pane.*;

import java.util.Hashtable;

public class Main extends Application {
    private static Scene scene;


    public static User u;

    public static Scene getScene(){
        return scene;
    }

    public void start(Stage primaryStage) throws Exception{
        primaryStage.setMaximized(true);
        scene = new Scene(new loginPane(), 700, 450);

        primaryStage.setTitle("NBA");
        primaryStage.setScene(scene);
        primaryStage.show();
        scene.getStylesheets().add(Main.class.getResource("style.css").toExternalForm());
    }

    public static void main(String[] args) {
        launch(args);
    }
}
