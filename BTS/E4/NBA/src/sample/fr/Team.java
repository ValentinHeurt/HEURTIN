package sample.fr;

public class Team {
    private int id;
    private String nom;
    private String conf;
    private String nick;
    private String logo;
    private int victoire;
    private int defaite;



    public Team(int id, String nom, String conf, int victoire, int defaite, String nick, String logo){
        this.id = id;
        this.nom = nom;
        this.conf = conf;
        this.victoire = victoire;
        this.defaite = defaite;
        this.nick = nick;
        this.logo = logo;



    }
    public Team(){}

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getConf() {
        return conf;
    }

    public int getVictoire() {
        return victoire;
    }

    public void setVictoire(int victoire) {
        this.victoire = victoire;
    }

    public int getDefaite() {
        return defaite;
    }

    public void setDefaite(int defaite) {
        this.defaite = defaite;
    }

    public void setConf(String conf) {
        this.conf = conf;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", conf='" + conf + '\'' +
                '}';
    }
}
