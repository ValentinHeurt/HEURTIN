package sample.fr;

import java.sql.Timestamp;

public class User {
    private int id;
    private String pseudo;
    private int grade;
    private String photo;
    private String email;
    private Timestamp timestamp;



    public User(){}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User(int id, String pseudo, int grade, String photo, String email){
        this.id = id;
        this.pseudo = pseudo;
        this.grade  = grade;
        this.photo  = photo;
        this.email  = email;


    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
