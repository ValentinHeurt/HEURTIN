package sample.fr;

public class Joueur {
    private int id;
    private String nom;
    private String prenom;
    private int age;
    private Team team;
    private int totalPoints;
    private int totalMin;
    private float moyMin;
    private float moyPts;
    private float pcTirs;
    private float pc3pt;
    private float moyReb;
    private float moyAst;
    private int totalAst;
    private int totalReb;
    private float moyStl;
    private int totalStl;
    private float moyBLK;
    private int totalBLK;
    private float moyPF;
    private int totalPF;

    public Joueur(int id, String nom, String prenom, int age, Team team, int totalPoints, int totalMin, float moyMin, float moyPts, float pcTirs, float pc3pt, float moyReb, float moyAst,int totalAst, int totalReb, float moyStl, int totalStl, float moyBLK, int totalBLK, float moyPF, int totalPF){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.team = team;
        this.totalPoints = totalPoints;
        this.totalMin = totalMin;
        this.moyMin = moyMin;
        this.moyPts = moyPts;
        this.pcTirs = pcTirs;
        this.pc3pt = pc3pt;
        this.moyReb = moyReb;
        this.moyAst = moyAst;
        this.totalAst = totalAst;
        this.totalReb = totalReb;
        this.moyStl = moyStl;
        this.totalStl = totalStl;
        this.moyBLK = moyBLK;
        this.totalBLK = totalBLK;
        this.moyPF = moyPF;
        this.totalPF = totalPF;
    }
    public Joueur(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public int getTotalMin() {
        return totalMin;
    }

    public void setTotalMin(int totalMin) {
        this.totalMin = totalMin;
    }

    public float getMoyMin() {
        return moyMin;
    }

    public void setMoyMin(float moyMin) {
        this.moyMin = moyMin;
    }

    public float getMoyPts() {
        return moyPts;
    }

    public void setMoyPts(float moyPts) {
        this.moyPts = moyPts;
    }

    public float getPcTirs() {
        return pcTirs;
    }

    public void setPcTirs(float pcTirs) {
        this.pcTirs = pcTirs;
    }

    public float getPc3pt() {
        return pc3pt;
    }

    public void setPc3pt(float pc3pt) {
        this.pc3pt = pc3pt;
    }

    public float getMoyReb() {
        return moyReb;
    }

    public void setMoyReb(float moyReb) {
        this.moyReb = moyReb;
    }

    public float getMoyAst() {
        return moyAst;
    }

    public void setMoyAst(float moyAst) {
        this.moyAst = moyAst;
    }

    public int getTotalAst() {
        return totalAst;
    }

    public void setTotalAst(int totalAst) {
        this.totalAst = totalAst;
    }

    public int getTotalReb() {
        return totalReb;
    }

    public void setTotalReb(int totalReb) {
        this.totalReb = totalReb;
    }

    public float getMoyStl() {
        return moyStl;
    }

    public void setMoyStl(float moyStl) {
        this.moyStl = moyStl;
    }

    public int getTotalStl() {
        return totalStl;
    }

    public void setTotalStl(int totalStl) {
        this.totalStl = totalStl;
    }

    public float getMoyBLK() {
        return moyBLK;
    }

    public void setMoyBLK(float moyBLK) {
        this.moyBLK = moyBLK;
    }

    public int getTotalBLK() {
        return totalBLK;
    }

    public void setTotalBLK(int totalBLK) {
        this.totalBLK = totalBLK;
    }

    public float getMoyPF() {
        return moyPF;
    }

    public void setMoyPF(float moyPF) {
        this.moyPF = moyPF;
    }

    public int getTotalPF() {
        return totalPF;
    }

    public void setTotalPF(int totalPF) {
        this.totalPF = totalPF;
    }

    @Override
    public String toString() {
        return "Joueur{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", age=" + age +
                ", team=" + team +
                ", totalPoints=" + totalPoints +
                ", totalMin=" + totalMin +
                ", moyMin=" + moyMin +
                ", moyPts=" + moyPts +
                ", pcTirs=" + pcTirs +
                ", pc3pt=" + pc3pt +
                ", moyReb=" + moyReb +
                ", moyAst=" + moyAst +
                ", totalAst=" + totalAst +
                ", totalReb=" + totalReb +
                ", moyStl=" + moyStl +
                ", totalStl=" + totalStl +
                ", moyBLK=" + moyBLK +
                ", totalBLK=" + totalBLK +
                ", moyPF=" + moyPF +
                ", totalPF=" + totalPF +
                '}';
    }
}
