<?php 
require 'vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Firebase\JWT\JWT;
$app = new \Slim\App;
error_reporting(E_ALL);
ini_set('display_errors', 1);
$app->get('/obtentionToken', function(Request $request, Response $response){  
  //vérification de l'utilisateur
  $tb = $request->getQueryParams();
  $login = $tb['login'];
  $pass = $tb['pass'];  
  $allowed= checkUser($login,$pass);
  if($allowed){
    $token=getTokenJWT();
    return $response->withJson($token,200);
  }else{
    return $response->withStatus(401);
  }
});
$app->get('/zaza', function(Request $request, Response $response){  
return "wazaaaaaaaa";
});
$app->get('/bonjour', function(Request $request, Response $response){ 
return "coucou";
});
$app->get('/personnage/{name}', function(Request $request, Response $response){
$name = $request->getAttribute('name');
return getPersonnage($name);
});
$app->get('/users', function(request $request, Response $response){
  return getUsers();
});
$app->get('/personnages', function(request $request, Response $response){
  return firstH();
});
$app->post('/user', function(Request $request, Response $response){
   $tb = $request->getQueryParams();
    $id = $tb["id"];
    $mdp = $tb["mdp"];
        //fonction de vérification d'utilisateur
  return checkUser($id, $mdp);
});
$app->delete('/user', function(Request $request, Response $response){
  $tb = $request->getQueryParams();
  $token = $tb['token'];
  if(validJWT($token)){
  $login = $tb['login'];
  return deleteUser($login);
}
else{
  return $response->withStatus(401);
}
});
$app->delete('/ville', function(Request $request, Response $response){
  $tb = $request->getQueryParams();
  $token = $tb['token'];
  if(validJWT($token)){
    $ville = $tb['ville'];
    return deleteVille($ville);
  }
  else{
    return $response->withStatus(401);
  }
});
$app->put('/user', function(Request $request, Response $response){
  $tb = $request->getQueryParams();
  $token = $tb["token"]; 
  if(validJWT($token)){
      $user = $tb["user"];
      $mail = $tb["mail"];
      return changeMail($user, $mail);

  }else{
    return $response->withStatus(401);
  }

         
});
$app->get('/nbperso', function(Request $request, Response $response){
  return getPersoByHouses();  

});
function connexion()
{
/*IBM Cloud
* $vcap_services = json_decode($_ENV['VCAP_SERVICES'], true);
* $uri = $vcap_services['compose-for-mysql'][0]['credentials']['uri'];
* $db_creds = parse_url($uri);
* $dbname = "patisserie";
* $dsn = "mysql:host=" . $db_creds['host'] . ";port=" . $db_creds['port'] . ";dbname=" . $dbname;
* return $dbh = new PDO($dsn, $db_creds['user'], $db_creds['pass'],array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
* */
//autre
return $dbh = new PDO("mysql:host=valentin-heurtin.cueollcmbqic.eu-west-1.rds.amazonaws.com:3306;dbname=got", 'admin', 'lolilol123456', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
}
function getTokenJWT() {
 // Make an array for the JWT Payload
  $payload = array(
    //30 min
    "exp" => time() + (60 * 30)
  );
   // encode the payload using our secretkey and return the token
  return JWT::encode($payload, "secret");
}
  function validJWT($token) {
    $res = false;
    try {
        $decoded = JWT::decode($token, "secret", array('HS256'));       
    } catch (Exception $e) {
      return $res;
    }
    $res = true;
    return $res;  
  }
function getPersonnage($name)
{
$sql = "SELECT * FROM characters WHERE  name = '".$name."'";
try{
$dbh=connexion();
$statement = $dbh->prepare($sql);
$statement->execute();
$result = $statement->fetchAll(PDO::FETCH_CLASS); 
return json_encode($result, JSON_PRETTY_PRINT);
} catch(PDOException $e){
return '{"error":'.$e->getMessage().'}';
}
}
function getPersoByHouses(){
  $sql = "SELECT houses.name, COUNT(characters.name) as nb FROM characters,houses WHERE houses.id = characters.house GROUP BY houses.name HAVING COUNT(characters.name) >9";
  try{
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_CLASS);
    return json_encode($result, JSON_PRETTY_PRINT);
  } catch(PDOException $e){
    return '{"error":'.$e->getMessage().'}';
  }
}
function checkUser($id, $mdp)
{
  $sql = "SELECT mail FROM user WHERE login='".$id."' AND mdp='".$mdp."'";
  try{
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_CLASS); 
    foreach ($result as $ligne) {
      return $ligne->mail;

     }    

                 
  } catch(PDOException $e){
    return '{"error":'.$e->getMessage().'}';
  }
}
$app->get('/tranchePerso',function(Request $request, Response $response){
  return getTranchePerso();
});
function getTranchePerso(){
  try{
      $dbh=connexion();
      $sql = "call getTranchePerso();";
      $statement = $dbh->prepare($sql);
      $statement->execute();
      $result = $statement->fetchAll(PDO::FETCH_CLASS);
      return json_encode($result, JSON_PRETTY_PRINT);

  } catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';

  }

}
$app->post('/sign', function(Request $request, Response $response){
  $tb = $request->getQueryParams();
  $token = $tb['token'];
  if(validJWT($token)){
      $id = $tb["login"];
      $mdp = $tb["mdp"];  
      $mail = $tb['mail'];
      return insertUser($id, $mdp, $mail);

    }
    else{
      return $response->withStatus(401);
    }
    });
function insertUser($id, $mdp, $mail)
    {
      try{
      $dbh=connexion();
      $sql = "INSERT INTO user (login,mdp,mail) VALUES ('".$id."','".$mdp."','".$mail."')";
      $statement = $dbh->prepare($sql);
      $statement->execute();
      return "Inscription réussite !";
  } catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';

  }

    }
function deleteUser($login){
  try{
    $dbh=connexion();
    $sql = "DELETE FROM user WHERE login='".$login."'";
    $statement = $dbh->prepare($sql);
    $statement->execute();
    return "User supprimé avec succes";
  }catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
}}
function deleteVille($ville){
  try{
    $dbh=connexion();
    $sql = "DELETE FROM cities WHERE name='".$ville."'";
    $statement = $dbh->prepare($sql);
    $statement->execute();
    return "Ville supprimée avec succes";
  }catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
}
  }

function changeMail($user, $mail){
  try{
    $dbh=connexion();
    $sql = "UPDATE user SET mail='".$mail."' WHERE login='".$user."'";
    $statement = $dbh->prepare($sql);
    $statement->execute();
    return "mail modifié";
  }catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
}
}
function firstH(){
  try{
    $dbh=connexion();
    $sql="SELECT name,culture,titles FROM characters LIMIT 100;";
    $statement = $dbh->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_CLASS); 
    return json_encode($result, JSON_PRETTY_PRINT);


  }catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
}
}
function getUsers(){
  try{
    $dbh=connexion();
    $sql="SELECT * FROM user";
    $statement = $dbh->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_CLASS);
    return json_encode($result, JSON_PRETTY_PRINT);
  }catch(PDOException $e){
            return '{"error":'.$e->getMessage().'}';

  }
}
$app->get('/statsville', function(Request $request, Response $response){
  return getVilleByType();  

});
function getVilleByType(){
  $sql = "SELECT cities_type.name, COUNT(cities.id) as nb FROM cities, cities_type WHERE cities.type = cities_type.id GROUP BY cities_type.name;";
  try{
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_CLASS);
    return json_encode($result, JSON_PRETTY_PRINT);
  } catch(PDOException $e){
    return '{"error":'.$e->getMessage().'}';

}}

$app->run();
