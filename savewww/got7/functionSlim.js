var nombrePerso;
var nombreVille;
$(document ).ready(function() {

     $("#btn-valide4").click(function(event) {               
                   $.ajax({ 
                    type: "GET",
                    url: "http://localhost/got7/index.php/obtentionToken",
                    data: "login="+ $("#login").val() +"&pass="+ $("#pass").val(),
                    success: function(data){
                          sessionStorage.setItem('token', data);
                          document.location.href="http://localhost/got7/got5.html";                              
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {      
                            $(".form-group-password").addClass("has-danger");
                            $("#password").addClass("form-control-danger");
                            alert("Ce compte n'existe pas");
                    }             
                    });
    });
     $('#btn-valide').click(function(){ 
    let login=$('#login').val();
      $.ajax({ 
      type: "DELETE",
      url: "http://localhost/got7/index.php/user?token="+sessionStorage.getItem("token")+"&login="+login,
      success: function(data){  
        alert(data);
      },
      error: function(){
        alert("Vous n'êtes pas log");
      }
    });
  });
  $('#btn-valide2').click(function(){
    let user=$('#user').val();
    let mail=$('#mail').val();
      $.ajax({
        type: "PUT",
        url: "http://localhost/got7/index.php/user?user="+user+"&mail="+mail+"&token="+sessionStorage.getItem('token'),
        success: function(data){
          alert(data);
        },
        error: function(){
        alert("Vous n'êtes pas log");
      }
      });
  });
  $('#btn-valide3').click(function(){
    $('#result3').bootstrapTable({
url: 'http://localhost/got7/index.php/users',
columns: [{
field: 'id',
title: 'id'
}, {
field: 'login',
title: 'Pseudo'
},
]
});
  });
    $('#btn-add').click(function(){ 
 let login=$('#login-add').val();
 let mdp=$('#mdp-add').val();
 let mail=$('#mail-add').val();
 if(login!=null && mdp!=null && mail!=null && login!="" && mdp!="" && mail!=""){
 $.ajax({ 
    type: "POST",
    contentType: 'application/json; charset=utf-8',
    url: "http://localhost/got7/index.php/sign?login="+login+"&mdp="+mdp+"&mail="+mail+"&token="+sessionStorage.getItem("token"),
    success: function(data){
       alert(data);
       },
       error: function(){
        alert("Vous n'êtes pas log");
       }
     });
}else{
  alert("Veuillez entrer toute les informations");
}
});
    $('#btn-delete-ville').click(function(){
      let ville=$('#ville').val();
      $.ajax({ 
      type: "DELETE",
      url: "http://localhost/got7/index.php/ville?token="+sessionStorage.getItem("token")+"&ville="+ville,
      success: function(data){  
        alert(data);
      },
      error: function(){
        alert("Vous n'êtes pas log");
      }
    });

    });
    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    };
     $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        url: "http://localhost/got7/index.php/nbperso",
        success: function(data){
            nombrePerso = JSON.parse(data);
        }

        });
    $('#btn-stats').click(function(){
        
        var nb = new Array();
        var maison = new Array();
        var backgroundColor = new Array();
        for (var i = 0; i < nombrePerso.length; i++) {
          nb[i] = nombrePerso[i].nb;
          maison[i] = nombrePerso[i].name;
          backgroundColor[i] = getRandomColor();
        }
        var ctx = document.getElementById("stats").getContext('2d');
        var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: maison,
            datasets: [{
                label: 'Perso par maison',
                data: nb,
                backgroundColor: backgroundColor
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
    });
         $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        url: "http://localhost/got7/index.php/statsville",
        success: function(data){
            nombreVille = JSON.parse(data);
        }

        });
    $('#btn-stats-ville').click(function(){
        
        var nb = new Array();
        var ville = new Array();
        var backgroundColor = new Array();
        for (var i = 0; i < nombreVille.length; i++) {
          nb[i] = nombreVille[i].nb;
          ville[i] = nombreVille[i].name;
          backgroundColor[i] = getRandomColor();
        }
        var ctx = document.getElementById("statsVille").getContext('2d');
        var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ville,
            datasets: [{
                label: 'Nombre de ville de type',
                data: nb,
                backgroundColor: backgroundColor
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
    });
    $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        url: "http://localhost/got7/index.php/tranchePerso",
        success: function(data){
            tranchePerso = JSON.parse(data);
        }

        });
    $('#btn-stats-perso').click(function(){
        
        var nb = new Array();
        var backgroundColor = new Array();
        for (var i = 0; i < nombreVille.length; i++) {
          nb[i] = tranchePerso[i].nb;
          backgroundColor[i] = getRandomColor();
        }
        var ctx = document.getElementById("statsPerso").getContext('2d');
        var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['-50 à 0', '1 à 50', '51 à 100', '101 à 150', '151 à 200', '201 à 250', '251 à 300'],
            datasets: [{
                label: 'Nombre de perso par tranche de 50 ans',
                data: nb,
                backgroundColor: backgroundColor
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
    });
});