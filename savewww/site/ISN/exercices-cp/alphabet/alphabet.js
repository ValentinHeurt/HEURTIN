var l=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
var L=[] //liste aléatoire
var liste = function (){
    var let = document.getElementById("gen");
    let.addEventListener("click",gene);
    var correct = document.getElementById("correc");
    correct.addEventListener("click",correction);
}
window.addEventListener("load",liste);
var gene = function(){
    for(var i = 0;i<10;i++){
        L[i]=l[Math.ceil(Math.random()*25)];
    }
    document.getElementById("generation").innerHTML = L;
	document.getElementById("repCorrec").innerHTML = "";
    document.getElementById("repCorrec2").innerHTML = "";
    
}
window.addEventListener("load",gene);
var correction = function(){
     var nombv = 0;
     var nombc = 0;
    for (var i = 0; i < 10; i++) {
        var lettre = L[i];
        if ((lettre === "A") || (lettre === "E") || (lettre === "I") ||
            (lettre === "O") || (lettre === "U") || (lettre === "Y")) {
            nombv++;
        }
        else{
            nombc++; 
        }
}
    var rep = document.getElementById("rep").value
    var rep2 = document.getElementById("rep2").value
    if((rep2 == nombv)&&(rep == nombc)){
        document.getElementById("repCorrec").style.color = "green";
        document.getElementById("repCorrec").innerHTML = "Bien joué, tu as trouvé la bonne réponse !";
        document.getElementById("repCorrec2").innerHTML = "";
    }
    else{
         document.getElementById("repCorrec").style.color = "red";
         document.getElementById("repCorrec").innerHTML = "Dommage, tu as du faire une erreur !";
         document.getElementById("repCorrec2").innerHTML = "La bonne réponse était : " + nombc + " consonnes " + nombv + " voyelles.";
    }
}
