<?php
require_once('conn.php'); // On se connecte a la base de donnée
$sql = $dbh->query("SELECT batiment,etage,zone,statut FROM ENSEA WHERE ajd = CURDATE()") // requête SQL regroupant les réponses de la journée
?>
<TABLE BORDER>
	<TR>
		<TH>Batiment</TH><TH>Etage</TH><TH>Statut</TH>
	</TR>
<?php 

$bat = ["A","C","D"];
$eta = ["RDC","1er","2eme","3eme"];
$zone = ["Zone 1","Zone 2","Zone 3","Zone 4","Zone 5"];
$etaD = ["Rez de jardin","Rez de passerelle","2eme","3eme"];
$flag = false;
$donnees = $sql->fetchAll();
// Création du tableau qui récupère les informations du jour même
for($i=0; $i<2;$i++){
		for($j=0;$j<4;$j++){
			echo "<TR>";
            echo "<TD>".$bat[$i]."</TD>";
			echo "<TD>".$eta[$j]."</TD>";

			for($k=0;$k<count($donnees);$k++){
				if($donnees[$k]['batiment']!='D'){
				if($donnees[$k]['batiment'] == $bat[$i] and $donnees[$k]['etage'] == $eta[$j]){
			    	if($donnees[$k]['statut']=="valide"){
				    	echo "<TD bgcolor='#00ff00'>   </TD>";
						$flag = true;
					}
					else{
						if($donnees[$k]['statut']=="invalide"){
							echo "<TD bgcolor='#ff0000'>   </TD>";
							$flag = true;
						}
					}
				}  
			    }

		    }


	    	if($flag == false){
				echo "<TD>En attente</TD>";

			}
			else{
				$flag = false;
			}
	        }

	        echo "</TR>";


		}
	echo "</TABLE>";
	?>
<TABLE BORDER>
	<TR>
		<TH>Batiment</TH><TH>Etage</TH><TH>Zone</TH><TH>Statut</TH>
	</TR>
<?php
for($i=0;$i<2;$i++){
	for($j=0;$j<3;$j++){
		echo "<TR>";
		echo "<TD>D</TD>";
        echo "<TD>".$etaD[$i]."</TD>";
		echo "<TD>".$zone[$j]."</TD>";
			for($k=0;$k<count($donnees);$k++){
				if($donnees[$k]['batiment']=='D'){
				if($donnees[$k]['etage'] == $etaD[$i] and $donnees[$k]['zone']==$zone[$j]){
			    	if($donnees[$k]['statut']=="valide"){
				    	echo "<TD bgcolor='#00ff00'>   </TD>";
						$flag = true;
					}
					else{
						if($donnees[$k]['statut']=="invalide"){
							echo "<TD bgcolor='#ff0000'>   </TD>";
							$flag = true;
						}
						
					}
				}  
			    }

		    }
		    if($flag == false){
				echo "<TD>En attente</TD>";

			}
			else{
				$flag = false;
			}
		    




	}
	echo "</TR>";
}
	for($j=0;$j<5;$j++){
		echo "<TR>";
		echo "<TD>D</TD>";
        echo "<TD>2eme</TD>";
		echo "<TD>".$zone[$j]."</TD>";
			for($k=0;$k<count($donnees);$k++){
				if($donnees[$k]['batiment']=='D'){
				if($donnees[$k]['etage'] == "2eme" and $donnees[$k]['zone']==$zone[$j]){
			    	if($donnees[$k]['statut']=="valide"){
				    	echo "<TD bgcolor='#00ff00'>   </TD>";
						$flag = true;
					}
					else{
						if($donnees[$k]['statut']=="invalide"){
							echo "<TD bgcolor='#ff0000'>   </TD>";
							$flag = true;
						}
						
					}
				}  
			    }

		    }
		    if($flag == false){
				echo "<TD>En attente</TD>";

			}
			else{
				$flag = false;
			}
		    


     echo "</TR>";

	}
	for($j=0;$j<3;$j++){
		echo "<TR>";
		echo "<TD>D</TD>";
        echo "<TD>3eme</TD>";
		echo "<TD>".$zone[$j]."</TD>";
			for($k=0;$k<count($donnees);$k++){
				if($donnees[$k]['batiment']=='D'){
				if($donnees[$k]['etage'] == "2eme" and $donnees[$k]['zone']==$zone[$j]){
			    	if($donnees[$k]['statut']=="valide"){
				    	echo "<TD bgcolor='#00ff00'>   </TD>";
						$flag = true;
					}
					else{
						if($donnees[$k]['statut']=="invalide"){
							echo "<TD bgcolor='#ff0000'>   </TD>";
							$flag = true;
						}
						
					}
				}  
			    }

		    }
		    if($flag == false){
				echo "<TD>En attente</TD>";

			}
			else{
				$flag = false;
			}
		    


     echo "</TR>";

	}

echo "</TABLE>";



?>

</br>



<form method="post" id="choix">
<label for="date">Selectionnez une date pour voir l'historique de l'évacuation correspondante.</label>
<select name="choixDate" id="choixDate">
<?php
// Historique des anviens exercices
$sql2 = $dbh->query("SELECT ajd,batiment,etage,zone,statut FROM ENSEA"); // requête SQL regroupant les réponses de la journée
$avant = array();
$flag = 1;
while ($donnees = $sql2->fetch())
{
	for($i = 0; $i<count($avant);$i++){
		if($avant[$i] == $donnees['ajd']){
			$flagos = 0;
		}
	}
	if($flagos == 1){
?>
           <option value=" <?php echo $donnees['ajd']; ?>"> <?php echo $donnees['ajd']; ?></option>
<?php
}
array_push($avant, $donnees['ajd']);
$flagos = 1;
}
$sql2->closeCursor();
?>
</select>
<button class="btn" id="btn">Valider</button>
</form>


<TABLE BORDER>
	<TR>
		<TH>Date</TH><TH>Heure</TH><TH>Batiment</TH><TH>Etage</TH><TH>Statut</TH>
	</TR>
<?php
$choixDate = $_POST['choixDate'];
$response = $dbh->prepare("SELECT ajd,heure,batiment,etage,zone,statut FROM ENSEA WHERE ajd = ?"); // requête SQL
$response->bindParam(1,$choixDate);
$response->execute();
while($data = $response->fetch()){
?>
<TR>
	<TD><?php echo $data['ajd'] ?></TD><TD><?php echo $data['heure']; ?></TD><TD><?php echo $data['batiment']; ?></TD><TD><?php echo $data['etage']; ?></TD><?php
	 if($data['statut'] == "valide"){
	 	echo "<TD bgcolor='#00ff00'></TD>";
	 }
	 else{
	 	if($data['statut'] == "invalide"){
	 		echo "<TD bgcolor='#ff0000'></TD>";

	 	}
	 	else{
	 		echo "<TD>En attente</TD";
	 	}
	 }
	 ?>	
</TR>
<?php
}
?>
</TABLE>
<?php
$response->closeCursor();
?>